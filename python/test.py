from unittest import TestCase
import unittest
import ford_fulkerson

class FFTest(unittest.TestCase):
    def test_algo(self):
        g = ford_fulkerson.FlowNetwork()
        [g.add_vertex(v) for v in "sopqrt"]
        g.add_edge('s','o',3)
        g.add_edge('s','p',3)
        g.add_edge('o','p',2)
        g.add_edge('o','q',3)
        g.add_edge('p','r',2)
        g.add_edge('r','t',3)
        g.add_edge('q','r',4)
        g.add_edge('q','t',2)
        print (g.max_flow('s','t'))

if __name__ == '__main__':
    unittest.main()

