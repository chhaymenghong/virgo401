# CSE401 Visual Language Interpreter #

Live version is hosted here: http://students.washington.edu/fge3/
Currently in testing and may not be working completely.

### Links ###

* Project Proposal: https://drive.google.com/open?id=10FqZFefCVWTeU-tknZJsp3YeuLXf7_u32J4eH4ySeY4
* Peer Review: https://drive.google.com/open?id=16Mr_zvQHZipdHte0GEPRPOYYrdYxx1jlDhcASvh2JFk
* Language Reference: https://drive.google.com/open?id=1YUJKA-lyds1ZUIDx-c08_PRxhkoBOcSa_XXGTRaI5uM
* Design Doc: https://drive.google.com/open?id=1DF37VG7FFqY9DlB55FN8jA-l1d-XbxNzV5cCypDjgNg
* Poster: https://drive.google.com/a/cs.washington.edu/file/d/0B_OlqRBJdEnXUUVOZThJa29lN0E/view?usp=sharing
* Source Code: https://drive.google.com/a/cs.washington.edu/file/d/0B_OlqRBJdEnXb2JGRTZPeUxDTDA/view?usp=sharing
* Examples: (Go to the live hosted version and click on "Samples")
* Screencast: https://youtu.be/UBuYeeslJX0