define(function() {
  function Graph(){
    var self = this;

  	this.isWeighted=true;
  	this.nodes=[];

  	this.addNode=addNode;
  	this.removeNode=removeNode;
    this.removeEdge=removeEdge;
    this.setEdgeD=setEdgeD;
  	this.nodeExist=nodeExist;
  	this.getAllNodes=getAllNodes;
    this.findNodeBasedOnRawData=findNodeBasedOnRawData;
    this.getNodeWithLabel=getNodeWithLabel;
    this.getNeighbor=getNeighbor;
    this.getEdge = getEdge;

    function findNodeBasedOnRawData(node401) {
      var nodes = this.nodes;
      var len = nodes.length;
      for (var i = 0; i < len; i++) {
        var gNode = nodes[i];
        var gNodeRawData = gNode.getRawData();
        if (gNodeRawData === node401) {
          return gNode;
        }
      }
      return null;
    }
    // take in rawData
  	function addNode(node401){
  		var gNode = new Node(node401);
  		this.nodes.push(gNode);
  		return gNode.getRawData();
  	}
    // take in rawData, but we only store gNode
  	function removeNode(node401){
      var gNode = findNodeBasedOnRawData.call(this, node401);
      if (gNode !== null) {
        index=this.nodes.indexOf(gNode);
        if(index>-1){
          this.nodes.splice(index,1);
          len=this.nodes.length;

          for (var i = 0; i < len; i++) {
            if(this.nodes[i].adjList.indexOf(gNode)>-1){
              this.nodes[i].adjList.slice(this.nodes[i].adjList.indexOf(gNode));
              this.nodes[i].weight.slice(this.nodes[i].adjList.indexOf(gNode));
            }
          }
        }
      }
  	}
  	function nodeExist(node401) {
      var gNode = findNodeBasedOnRawData.call(this,node401);

  		index=this.nodes.indexOf(gNode);
  		if(index>-1){
  			return true;
  		}
  		return false;
  	}

    // Note: this also generate log coz of call to label()
    function getNodeWithLabel(label) {
      var allNode = this.nodes;
      var numNodes = allNode.length;
      for (var i = 0; i < numNodes; i++) {
        var rawDataNode = allNode[i].getRawData();
        if (rawDataNode.label === label) {
          return rawDataNode;
        }
      }
      return null;
    }

    this.getGNodeWithLabel = function(label) {
      var allNode = this.nodes;
      var numNodes = allNode.length;
      for (var i = 0; i < numNodes; i++) {
        var rawDataNode = allNode[i].getRawData();
        if (rawDataNode.label === label) {
          return allNode[i];
        }
      }
      return null;
    }

  	function getAllNodes(){
      var listOfNodesAsRawData = [];
      var len = this.nodes.length;
      for (var i = 0; i < len; i++) {
        listOfNodesAsRawData.push(this.nodes[i].getRawData());
      }
      return listOfNodesAsRawData;
  	}
    function getEdge(node401_1, node401_2) {
      var gNode1 = self.getGNodeWithLabel(node401_1.label);
      var gNode2 = self.getGNodeWithLabel(node401_2.label);
      if (gNode1 !== null && gNode2 !== null) {
        return gNode1.getEdgeBetweenTwoNodes(gNode2);
      }
      return null;
    }
    function getNeighbor(node401) {
      var gNode = self.getGNodeWithLabel(node401.label);
      if (gNode !== null) {
        var o = [];
        for (var i = 0; i < gNode.adjList.length; i++) {
          o.push(gNode.adjList[i].rawData)
        }
        return o;
      } else {
        return null;
      }
    }
    function setEdgeD(node401_1, node401_2, node401_3) {
      var gNode1 = self.getGNodeWithLabel(node401_1.label);
      var gNode2 = self.getGNodeWithLabel(node401_2.label);
      // turn node3 into gNode
      var gNode3 = new Node(node401_3);

      if (gNode1 !== null && gNode2 !== null) {
        // check if there already is an edge between them
        var neighbors = gNode1.getAdjList();
        var len = neighbors.length;
        for (var i = 0; i < len; i++) {
          if (neighbors[i] === gNode2) {
            return null;
          }
        }
        gNode1.addEdge(gNode2, gNode3);
        return node401_3;
      } else {
        return null;
      }
    }
    function removeEdge(node401_1, node401_2) {
      var gNode1 = self.getGNodeWithLabel(node401_1.label);
      var gNode2 = self.getGNodeWithLabel(node401_2.label);
      if (gNode1 !== null && gNode2 !== null) {
        // check if there is edge between them
        var adjList = gNode1.getAdjList();
        var len = adjList.length;
        for (var i = 0; i < len; i++) {
          var neighbour = adjList[i];
          if (neighbour === gNode2) {
            var edgeNode = gNode1.weight[i];
            edgeNode.removeEdgeBetweenTwoNeighbor(i);
            return edgeNode.getRawData();
          }
        }
        // there is no edge between them
        return null;
      } else {
        return null;
      }
    }
  }

  function Node(node){
  	this.rawData=node;
  	this.adjList=[];
  	this.weight=[];

  	this.addEdge=addEdge;
    this.getAdjList = getAdjList;
    this.getEdges = getEdges;
    this.getNeighbors = getNeighbors;
    this.compare=compare;
    this.removeNeighbor = removeNeighbor;
    this.removeWeight = removeWeight;
    this.getRawData = getRawData;
    this.getEdgeBetweenTwoNodes = getEdgeBetweenTwoNodes;
    this.removeEdgeBetweenTwoNeighbor= removeEdgeBetweenTwoNeighbor;

    function addEdge(neighbour,weight){
  		this.adjList.push(neighbour);
  		this.weight.push(weight);
  	}

  	function getAdjList(){
  		return this.adjList;
  	}
    function getNeighbors() {
      var listOfNeighbors = [];
      var len = this.adjList.length;
      for (var i = 0; i < len; i++) {
        listOfNeighbors.push(this.adjList[i].getRawData());
      }
      return listOfNeighbors;
    }
    function getEdges() {
      var listOfEdgesAsRawData = [];
      var len = this.weight.length;
      for (var i = 0; i < len; i++) {
        listOfEdgesAsRawData.push(this.weight[i].getRawData());
      }
      return listOfEdgesAsRawData;
    }
  	function compare(node2){
  		return this.weight-node2.weight;
  	}
    function removeNeighbor(index) {
      this.adjList.splice(index, 1);
    }
    function removeWeight(index) {
      this.weight.splice(index, 1);
    }
    function removeEdgeBetweenTwoNeighbor(index) {
      removeNeighbor.call(this, index);
      removeWeight.call(this, index);
    }
    function getRawData() {
      return this.rawData;
    }
    function getEdgeBetweenTwoNodes(gNode2) {
      var allEdgesFromN1ToN2 = this.getAdjList();
      var len = allEdgesFromN1ToN2.length;
      for (var i = 0; i < len; i++) {
        var neighbor = allEdgesFromN1ToN2[i];
        if (neighbor.getRawData().label === gNode2.getRawData().label) {
          return (this.weight[i]).getRawData();
        }
      }
      return null;
    }

  }
  return {
    Graph : Graph,
    Node: Node
  };
});
