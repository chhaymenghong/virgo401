define(["./LogEntry"], function(EntryModule) {
  function LogManager() {
    this.listOfAllOperations = [];
    this.uniqueIdForDataStructure = -1;
    this.uniqueIdForNode = -1;
  }

  function serialize(object) {
    if(!object) return "";
    return JSON.parse(JSON.stringify(object));
  }

  LogManager.prototype.getNewIdForNewDataStructure = function() {
    this.uniqueIdForDataStructure++;
    return this.uniqueIdForDataStructure;
  };
  LogManager.prototype.addNewLogEntry = function(dataStructureType, operationName, params, dataStructureId, node) {
    var entry = new EntryModule.LogEntry(dataStructureType, operationName, serialize(params), dataStructureId, serialize(node));
    this.listOfAllOperations.push(entry);
  };
  LogManager.prototype.getNewIdForNewNode = function() {
    this.uniqueIdForNode++;
    return this.uniqueIdForNode;
  };

  LogManager.prototype.getAllOperations = function() {
    var len = this.listOfAllOperations.length;
    var copyOfEntry = [];
    for (var i = 0; i < len; i++ ) {
      copyOfEntry.push(this.listOfAllOperations[i]);
    }
    return copyOfEntry;
  };
  LogManager.prototype.findAllOperationOfThisDataStructure = function(dataStructureId) {
    return this.listOfAllOperations.filter(function(entry) {
      return entry.getDataStructureId() === dataStructureId;
    });
  };
  LogManager.prototype.clearAllEntries = function() {
    this.listOfAllOperations = [];
    this.uniqueIdForDataStructure = -1;
    this.uniqueIdForNode = -1;
  };
  // Rquiresjs makes sure that we have only one copy of this.
  // This file will only runs onces and requireJs will maintain this closure and
  // access to the same instance of LogManager
  var LogManagerInstance = new LogManager();

  return {
    LogManagerInstance : LogManagerInstance
  };
});
