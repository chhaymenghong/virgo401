define(["../Utility/Constants"], function(Constants) {
  function LogEntry(dataStructureType, operationName, parameterObj, id, node) {
    this.dataStructureType = dataStructureType;
    this.operationName = operationName;
    this.parameterObj = parameterObj;
    this.node = node;
    if (dataStructureType !== Constants.DATA_STRUCTURE_NAME.NODE_401) {
      this.id = id;
    } else {
      this.nodeId = id;
    }
  }
  LogEntry.prototype.getDataStructureType = function() {
    return this.dataStructureType;
  };
  LogEntry.prototype.getOperationName = function() {
    return this.operationName;
  };
  LogEntry.prototype.getParamObject = function() {
    return this.parameterObj;
  };
  // Distinguish whether it is a data structure id or node id
  LogEntry.prototype.getDataStructureId = function() {
    return this.id;
  };
  // Distinguish whether it is a data structure id or node id
  LogEntry.prototype.getNodeId = function() {
    return this.nodeId;
  };
  // Just give me the non-undefined id
  LogEntry.prototype.getId = function() {
    return this.nodeId === undefined ? this.id : this.nodeId;
  };
  LogEntry.prototype.getNode = function() {
    return this.node;
  };
  return {
    LogEntry : LogEntry
  };
});
