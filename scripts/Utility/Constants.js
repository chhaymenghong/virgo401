define(function () {
    //Do setup work here
    var ARRAY_METHOD_NAME = {
      NEW: 'new',
      LENGTH: 'length',
      GET: 'get',
      PUT: 'put'
    };

    var LINKED_LIST_METHOD_NAME = {
      NEW: 'new',
      INSERT_AT: 'insertAt',
      REMOVE_AT: 'removeAt',
      LENGTH: 'length'
    };

    var STACK_METHOD_NAME = {
      NEW: 'new',
      IS_EMPTY: 'isEmpty',
      PEEK: 'peek',
      POP: 'pop',
      PUSH: 'push'
    };

    var QUEUE_METHOD_NAME = {
      NEW: 'new',
      DEQUEUE: 'dequeue',
      ENQUEUE: 'enqueue',
      PEEK: 'peek'
    };

    var GRAPH_METHOD_NAME = {
      NEW: 'new',
      RANDOM_SEED: 'randomSeed',
      ADD_NODE: 'addNode',
      REMOVE_NODE: 'removeNode',
      GET_NODE: 'getNode',
      GET_NODES: 'getNodes',
      SET_EDGE_D: 'setEdge',
      SET_EDGE_U: 'setEdgeU',
      REMOVE_EDGE: 'removeEdge',
      NEIGHBORS: 'neighbors',
      GET_EDGE: 'getEdge'
    };

    var NODE_METHOD_NAME = {
      NEW: 'new',
      LABEL: 'label',
      SELECT: 'select',
      DESELECT: 'deselect',
      SET: 'set',
      GET: 'get',
      REMOVE: 'remove'
    };


// for later



    var SET_METHOD_NAME = {
      NEW: 'new',
      ADD: 'add',
      CONTAINS: 'contains',
      IS_EMPTY: 'isEmpty',
      REMOVE: 'remove',
    };

    var DICTIONARY_METHOD_NAME = {
      NEW: 'new',
      GET: 'get',
      KEYS: 'keys',
      DELETE: 'delete',
      PUT: 'put',
    };

    var BINARY_SEARCH_TREE_METHOD_NAME = {
      NEW: 'new',
      ADD: 'add',
      CLEAR: 'clear',
      CONTAINS: 'contains',
      EQUALS: 'equals',
      FOR_EACH: 'forEach',
      HEIGHT: 'height',
      INORDER_TRAVERSAL: 'inorderTraversal',
      IS_EMPTY: 'isEmpty',
      LEVEL_TRAVERSAL: 'levelTraversal',
      MAXIMUM: 'maximum',
      MINIMUM: 'minimum',
      POSTORDER_TRAVERSAL: 'postorderTraversal',
      PREORODER_TRAVERSAL: 'preorderTraversal',
      REMOVE: 'remove',
      SIZE: 'size',
      TO_ARRAY: 'toArray',
      SELECT_AT_VALUE: 'selectAtValue'
    };


    var DATA_STRUCTURE_NAME = {
      ARRAY_401: 'Array401',
      STACK_401: 'Stack401',
      LINKED_LIST_401: 'LinkedList401',
      QUEUE_401: 'Queue401',
      SET_401: 'Set401',
      DICTIONARY_401: 'Dictionary401',
      BINARY_SEARCH_TREE_401: 'BinarySearchTree401',
      GRAPH_401: 'Graph401',
      NODE_401: 'Node401'
    };

    return {
      DATA_STRUCTURE_NAME:DATA_STRUCTURE_NAME,
      ARRAY_METHOD_NAME: ARRAY_METHOD_NAME,
      STACK_METHOD_NAME: STACK_METHOD_NAME,
      LINKED_LIST_METHOD_NAME: LINKED_LIST_METHOD_NAME,
      QUEUE_METHOD_NAME: QUEUE_METHOD_NAME,
      SET_METHOD_NAME: SET_METHOD_NAME,
      DICTIONARY_METHOD_NAME: DICTIONARY_METHOD_NAME,
      BINARY_SEARCH_TREE_METHOD_NAME: BINARY_SEARCH_TREE_METHOD_NAME,
      GRAPH_METHOD_NAME: GRAPH_METHOD_NAME,
      NODE_METHOD_NAME: NODE_METHOD_NAME
    };
});
