define(["../Utility/Constants", "../Log/LogManager"], function(Constants, LogManagerModule) {
  var logManagerInstance;

  function Node401(label) {
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.label = label;
    this.fields = {};
    this.nodeId = logManagerInstance.getNewIdForNewNode();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.NEW, {}, this.nodeId, this); // "this" is a reference to this new node object
  }
  Node401.prototype = {};
  Node401.prototype.label = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.LABEL, {}, this.nodeId, this); // "this" is a reference to this new node object
    return this.label;
  };
  Node401.prototype.select = function(pColor) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.SELECT, {color: pColor}, this.nodeId, this); // "this" is a reference to this new node object
  };
  Node401.prototype.deselect = function () {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.DESELECT, {}, this.nodeId, this); // "this" is a reference to this new node object
  };
  Node401.prototype.set = function (pField, pValue) {
    this.fields[pField] = pValue;
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.SET, {field: pField, value: pValue}, this.nodeId, this); // "this" is a reference to this new node object
  };
  Node401.prototype.get = function (pField) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.GET, {field: pField}, this.nodeId, this); // "this" is a reference to this new node object
    return this.fields[pField];
  };
  Node401.prototype.remove = function (pField) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.NODE_401, Constants.NODE_METHOD_NAME.REMOVE, {field: pField}, this.nodeId, this); // "this" is a reference to this new node object
    delete this.fields[pField];
  };

  return {
    Node401 : Node401
  };
});
