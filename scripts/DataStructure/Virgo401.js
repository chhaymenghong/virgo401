// define(["./Array401", "./BinarySearchTree401", "./Dictionary401", "./Graph401",
//  "./LinkedList401", "./Queue401", "./Set401", "./Stack401", "./Node401",  "../Log/LogManager" ], function(Array401Module, BinarySearchTree401Module, Dictionary401Module, Graph401Module,
//  LinkedList401Module, Queue401Module, Set401Module, Stack401Module, Node401Module, LogManagerModule) {
define(["./Array401", "./Graph401", "./LinkedList401", "./Queue401", "./Stack401", "./Node401", "../Log/LogManager"],
 function(Array401Module, Graph401Module, LinkedList401Module, Queue401Module, Stack401Module, Node401Module, LogManagerModule){
  var Virgo401 = {
    Array : Array401Module.Array401,
    Graph: Graph401Module.Graph401,
    LinkedList: LinkedList401Module.LinkedList401,
    Queue: Queue401Module.Queue401,
    Stack: Stack401Module.Stack401,
    Node: Node401Module.Node401,
    LogManager: LogManagerModule.LogManagerInstance
  };

  return {
    Virgo401 : Virgo401
  };
});
