define(["../Library/buckets", "../Utility/Constants", "../Log/LogManager"], function(buckets, Constants, LogManagerModule) {
  var logManagerInstance;
  function Queue401() {
    this.innerQueue = new buckets.Queue();
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.QUEUE_401, Constants.QUEUE_METHOD_NAME.NEW, {}, this.dataStructureId);
  }
  Queue401.prototype.constructor = Queue401;
  Queue401.prototype.enqueue = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.QUEUE_401, Constants.QUEUE_METHOD_NAME.ENQUEUE, {}, this.dataStructureId, node);
    return this.innerQueue.enqueue(node);
  };
  Queue401.prototype.dequeue = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.QUEUE_401, Constants.QUEUE_METHOD_NAME.DEQUEUE, {}, this.dataStructureId);
    return this.innerQueue.dequeue();
  };

  Queue401.prototype.peek = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.QUEUE_401, Constants.QUEUE_METHOD_NAME.PEEK, {}, this.dataStructureId);
    return this.innerQueue.peek();
  };
  Queue401.prototype.isEmpty = function() {
    return this.innerQueue.size() === 0 ? true : false;
  }

  return {
    Queue401 : Queue401
  };
});
