// Require js makes
define(["../Library/buckets", "../Utility/Constants", "../Log/LogManager"], function(buckets, Constants, LogManagerModule) {
  var logManagerInstance;
  function Dictionary401() {
    this.innerDictionary = new buckets.Dictionary();
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.DICTIONARY_401, Constants.DICTIONARY_METHOD_NAME.NEW, {}, this.dataStructureId);
  }
  Dictionary401.prototype.constructor = Dictionary401;
  Dictionary401.prototype.get = function(pKey) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.DICTIONARY_401, Constants.DICTIONARY_METHOD_NAME.GET, {key: pKey}, this.dataStructureId);
    return this.innerDictionary.get(pKey);
  };
  Dictionary401.prototype.keys = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.DICTIONARY_401, Constants.DICTIONARY_METHOD_NAME.KEYS, {}, this.dataStructureId);
    return this.innerDictionary.keys();
  };

  Dictionary401.prototype.delete = function(pKey) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.DICTIONARY_401, Constants.DICTIONARY_METHOD_NAME.DELETE, {key : pKey}, this.dataStructureId);
    return this.innerDictionary.remove(pKey);
  };

  Dictionary401.prototype.put = function(pKey, node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.DICTIONARY_401, Constants.DICTIONARY_METHOD_NAME.PUT, {key : pKey}, this.dataStructureId, node);
    return this.innerDictionary.set(pKey, node);
  };
  return {
    Dictionary401 : Dictionary401
  };
});
