
// Require js makes
define(["../Library/buckets", "../Utility/Constants", "../Log/LogManager"], function(buckets, Constants, LogManagerModule) {
  var logManagerInstance;
  function Stack401() {
    this.innerStack = new buckets.Stack();
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.STACK_401, Constants.STACK_METHOD_NAME.NEW, {}, this.dataStructureId);
  }
  Stack401.prototype.constructor = Stack401;
  Stack401.prototype.isEmpty = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.STACK_401, Constants.STACK_METHOD_NAME.IS_EMPTY, {}, this.dataStructureId);
    return this.innerStack.isEmpty();
  };
  Stack401.prototype.peek = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.STACK_401, Constants.STACK_METHOD_NAME.PEEK, {}, this.dataStructureId);
    return this.innerStack.peek();
  };
  Stack401.prototype.pop = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.STACK_401, Constants.STACK_METHOD_NAME.POP, {}, this.dataStructureId);
    return this.innerStack.pop();
  };
  Stack401.prototype.push = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.STACK_401, Constants.STACK_METHOD_NAME.PUSH, {}, this.dataStructureId, node);
    return this.innerStack.push(node);
  };
  return {
    Stack401 : Stack401
  };
});
