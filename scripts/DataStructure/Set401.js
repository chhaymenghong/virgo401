// Require js makes
define(["../Library/buckets", "../Utility/Constants", "../Log/LogManager"], function(buckets, Constants, LogManagerModule) {
  var logManagerInstance;
  function Set401() {
    this.innerSet = new buckets.Set();
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.SET_401, Constants.SET_METHOD_NAME.NEW, {}, this.dataStructureId);
  }
  Set401.prototype.constructor = Set401;

  Set401.prototype.add = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.SET_401, Constants.SET_METHOD_NAME.ADD, {}, this.dataStructureId, node);
    return this.innerSet.add(node);
  };
  Set401.prototype.contains = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.SET_401, Constants.SET_METHOD_NAME.CONTAINS, {}, this.dataStructureId, node);
    return this.innerSet.contains(node);
  };
  Set401.prototype.isEmpty = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.SET_401, Constants.SET_METHOD_NAME.IS_EMPTY, {}, this.dataStructureId);
    return this.innerSet.isEmpty();
  };
  Set401.prototype.remove = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.SET_401, Constants.SET_METHOD_NAME.REMOVE, {}, this.dataStructureId, node);
    return this.innerSet.remove(node);
  };
  return {
    Set401 : Set401
  };
});
