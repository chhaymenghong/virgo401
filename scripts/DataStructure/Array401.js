define(["../Utility/Constants", "../Log/LogManager"], function(Constants, LogManagerModule) {
  var logManagerInstance;
  function Array401(length) {
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.ARRAY_401, Constants.ARRAY_METHOD_NAME.NEW, {}, this.dataStructureId);
    this.length = length;
    this.innerArray = new Array(this.length);
  }
  Array401.prototype = {};
  Array401.prototype.constructor = Array401;
  Array401.prototype.put = function(pIndex, node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.ARRAY_401, Constants.ARRAY_METHOD_NAME.PUT, {index: pIndex}, this.dataStructureId, node);
    if (pIndex >= this.length) {
      return false;
    } else {
      this.innerArray[pIndex] = node;
      return true;
    }
  };
  Array401.prototype.get = function(pIndex) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.ARRAY_401, Constants.ARRAY_METHOD_NAME.GET, {index: pIndex}, this.dataStructureId);
    return this.innerArray[pIndex];
  };

  return {
    Array401 : Array401
  };
});
