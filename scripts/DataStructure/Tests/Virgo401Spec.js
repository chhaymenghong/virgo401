//**Note the path in require starts from the jasmineFramework directory
describe("Virgo401Spec", function() {
  var Virgo401;
  beforeEach(function(done) {
    require(["../../Virgo401"], function(Virgo401Module) {
        Virgo401 = Virgo401Module.Virgo401;
        done();
    });

  });

  it("should give access to all data structure", function() {
    expect("a").toBe("a");
  });

});
