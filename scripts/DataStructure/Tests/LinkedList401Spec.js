describe("LinkedList401", function() {
  var LinkedList401;
  var LogManagerInstance;
  var linkedListInstance;
  var ConstantsModule;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../LinkedList401", "../../../Utility/Constants"], function(LogManagerInstanceModule, LinkedList401Module, Constants) {
      LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
      LinkedList401 = LinkedList401Module.LinkedList401;
      ConstantsModule = Constants;
      done();
    });
  });
  beforeEach(function() {
    linkedListInstance = new LinkedList401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });
  it("should be empty when first created", function() {
    expect(linkedListInstance.size()).toBe(0);
  });
  it("can add a specified item", function() {
    linkedListInstance.add(1, 0);
    expect(linkedListInstance.size()).toBe(1);
    linkedListInstance.add(2);
    expect(linkedListInstance.size()).toBe(2);
  });
  it("can clear all entries", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(1);
    linkedListInstance.add(2);

    expect(linkedListInstance.size()).toBe(3);
    linkedListInstance.clear();
    expect(linkedListInstance.size()).toBe(0);
  });
  it("can check whether it contains a specified item", function() {
    linkedListInstance.add(1);
    expect(linkedListInstance.contains(1)).toBeTruthy();
    expect(linkedListInstance.contains(2)).toBeFalsy();
  });
  it("can check get an element at a specific index", function() {
    linkedListInstance.add(1);
    expect(linkedListInstance.elementAtIndex(0)).toBe(1);
    expect(linkedListInstance.elementAtIndex(2)).toBe(undefined);
  });
  it("can check it is equivalent to another linkedList", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(1);
    var linkedListInstance2 = new LinkedList401();
    linkedListInstance2.add(1);
    linkedListInstance2.add(1);
    expect(linkedListInstance.equals(linkedListInstance2)).toBeTruthy();
    linkedListInstance.add(3);
    expect(linkedListInstance.equals(linkedListInstance2)).toBeFalsy();
  });
  it("can get the first element", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(2);
    expect(linkedListInstance.first()).toBe(1);
  });
  it("can get the index of a specified element", function() {
    linkedListInstance.add(1);
    expect(linkedListInstance.indexOf(1)).toBe(0);
    expect(linkedListInstance.indexOf(2)).toBe(-1);
  });
  it("can check if it is empty", function() {
    expect(linkedListInstance.isEmpty()).toBeTruthy();
    linkedListInstance.add(1);
    expect(linkedListInstance.isEmpty()).toBeFalsy();
  });
  it("can get the last element", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(2);
    expect(linkedListInstance.last()).toBe(2);
  });
  it("can remove an element", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(2);
    linkedListInstance.remove(2);
    expect(linkedListInstance.size()).toBe(1);
    linkedListInstance.remove(1);
    expect(linkedListInstance.size()).toBe(0);
  });
  it("can remove an element at a specified index", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(2);
    var remove = linkedListInstance.removeElementAtIndex(0);
    expect(remove).toBe(1);
    expect(linkedListInstance.size()).toBe(1);
  });
  it("can reverse the itself", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(2);
    linkedListInstance.reverse();
    expect(linkedListInstance.elementAtIndex(0)).toBe(2);
    expect(linkedListInstance.elementAtIndex(1)).toBe(1);
  });
  it("can get its own array representaion", function() {
    linkedListInstance.add(1);
    linkedListInstance.add(2);
    var array = linkedListInstance.toArray();
    expect(array[0]).toBe(1);
    expect(array[1]).toBe(2);
  });
  it("can generate log entry for every operation defined", function() {
    var LINKED_LIST_METHOD_NAME = ConstantsModule.LINKED_LIST_METHOD_NAME;
    var DATA_STRUCTURE_NAME = ConstantsModule.DATA_STRUCTURE_NAME;
    var LinkedListMethodConstants = Object.keys(LINKED_LIST_METHOD_NAME);

    linkedListInstance.add(1);
    linkedListInstance.clear();
    linkedListInstance.contains(1);
    linkedListInstance.elementAtIndex(0);
    var linkedListInstance2 = new LinkedList401();
    linkedListInstance.equals(linkedListInstance2);
    linkedListInstance.first();
    linkedListInstance.indexOf(1);
    linkedListInstance.isEmpty();
    linkedListInstance.last();
    linkedListInstance.remove(1);
    linkedListInstance.removeElementAtIndex(0);
    linkedListInstance.reverse();
    linkedListInstance.size();
    linkedListInstance.toArray();

    var log = LogManagerInstance.findAllOperationOfThisDataStructure(linkedListInstance.dataStructureId);
    var numLog = log.length;
    expect(numLog).toBe(15);

    // check operation name

    // TODO: get rid of this hax once we know what to do with forEach
    var offSet = 0;
    log.forEach(function(eachLog, index, listOfLog) {
      var opName = LINKED_LIST_METHOD_NAME[LinkedListMethodConstants[index + offSet]];

      if (opName === 'forEach') {
        offSet++;
        opName = LINKED_LIST_METHOD_NAME[LinkedListMethodConstants[index + offSet]];
      }
      expect(eachLog.getOperationName()).toBe(opName);
    });
    var newEntry = log[0];
    expect(newEntry.getParamObject().length).toBe(0);
    expect(newEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var addEntry = log[1];
    expect(addEntry.getParamObject().length).toBe(2);
    expect(addEntry.getParamObject()[0]).toBe(1);
    expect(addEntry.getParamObject()[1]).toBe(undefined);
    expect(addEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var clearEntry = log[2];
    expect(clearEntry.getParamObject().length).toBe(0);
    expect(clearEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var containsEntry = log[3];
    expect(containsEntry.getParamObject().length).toBe(2);
    expect(containsEntry.getParamObject()[0]).toBe(1);
    expect(containsEntry.getParamObject()[1]).toBe(undefined);
    expect(containsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var elementAtIndexEntry = log[4];
    expect(elementAtIndexEntry.getParamObject().length).toBe(1);
    expect(elementAtIndexEntry.getParamObject()[0]).toBe(0);
    expect(elementAtIndexEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var equalsEntry = log[5];
    expect(equalsEntry.getParamObject().length).toBe(2);
    expect(equalsEntry.getParamObject()[0]).toBe(linkedListInstance2);
    expect(equalsEntry.getParamObject()[1]).toBe(undefined);
    expect(equalsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var firstEntry = log[6];
    expect(firstEntry.getParamObject().length).toBe(0);
    expect(firstEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var indexOfEntry = log[7];
    expect(indexOfEntry.getParamObject().length).toBe(2);
    expect(indexOfEntry.getParamObject()[0]).toBe(1);
    expect(indexOfEntry.getParamObject()[1]).toBe(undefined);
    expect(indexOfEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var isEmptyEntry = log[8];
    expect(isEmptyEntry.getParamObject().length).toBe(0);
    expect(isEmptyEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var lastEntry = log[9];
    expect(lastEntry.getParamObject().length).toBe(0);
    expect(lastEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var removeEntry = log[10];
    expect(removeEntry.getParamObject().length).toBe(2);
    expect(removeEntry.getParamObject()[0]).toBe(1);
    expect(removeEntry.getParamObject()[1]).toBe(undefined);
    expect(removeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var removeElementAtIndexEntry = log[11];
    expect(removeElementAtIndexEntry.getParamObject().length).toBe(1);
    expect(removeElementAtIndexEntry.getParamObject()[0]).toBe(0);
    expect(removeElementAtIndexEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var reverseEntry = log[12];
    expect(reverseEntry.getParamObject().length).toBe(0);
    expect(reverseEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var sizeEntry = log[13];
    expect(sizeEntry.getParamObject().length).toBe(0);
    expect(sizeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

    var toArrayEntry = log[14];
    expect(toArrayEntry.getParamObject().length).toBe(0);
    expect(toArrayEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.LINKED_LIST_401);

  });
});
