describe("Queue401", function() {
  var Queue401;
  var LogManagerInstance;
  var queueInstance;
  var ConstantsModule;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../Queue401", "../../../Utility/Constants"], function(LogManagerInstanceModule, Queue401Module, Constants) {
      LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
      Queue401 = Queue401Module.Queue401;
      // debugger;
      ConstantsModule = Constants;
      done();
    });
  });
  beforeEach(function() {
    queueInstance = new Queue401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });
  it("should be empty when first created", function() {
    expect(queueInstance.size()).toBe(0);
  });
  it("can add an item", function() {
    var addRes = queueInstance.add(2);
    expect(queueInstance.size()).toBe(1);
    expect(addRes).toBeTruthy();
    addRes = queueInstance.add(3);
    expect(queueInstance.size()).toBe(2);
    expect(addRes).toBeTruthy();
  });
  it("can clear all items", function() {
    queueInstance.add(2);
    queueInstance.add(2);
    queueInstance.add(2);
    expect(queueInstance.size()).toBe(3);
    queueInstance.clear();
    expect(queueInstance.size()).toBe(0);
  });
  it("can check if it contains an item", function() {
    queueInstance.add(2);
    expect(queueInstance.contains(2)).toBeTruthy();
    expect(queueInstance.contains(3)).toBeFalsy();
  });
  it("can dequeue an item", function() {
    queueInstance.add(2);
    queueInstance.add(3);
    expect(queueInstance.size()).toBe(2);
    var deqRes = queueInstance.dequeue();
    expect(deqRes).toBe(2);
    expect(queueInstance.size()).toBe(1);
    deqRes = queueInstance.dequeue();
    expect(deqRes).toBe(3);
    expect(queueInstance.size()).toBe(0);
  });
  it("can enqueue an item", function() {
    var res = queueInstance.enqueue(2);
    expect(res).toBeTruthy();
    queueInstance.enqueue(3);
    expect(queueInstance.size()).toBe(2);
    var dequeueRes = queueInstance.dequeue();
    expect(dequeueRes).toBe(2);
    dequeueRes = queueInstance.dequeue();
    expect(dequeueRes).toBe(3);
    expect(queueInstance.size()).toBe(0);
  });
  it("can check if it is equivalent to another queue", function() {
    var tempQ = new Queue401();
    queueInstance.enqueue(2);
    queueInstance.enqueue(3);
    tempQ.enqueue(2);
    tempQ.enqueue(3);
    var equal = queueInstance.equals(tempQ);
    expect(equal).toBeTruthy();
    tempQ.enqueue(10);
    equal = queueInstance.equals(tempQ);
    expect(equal).toBeFalsy();
  });

  // forEach

  it("can check if it is empty", function() {
    expect(queueInstance.isEmpty()).toBeTruthy();
    queueInstance.enqueue(1);
    expect(queueInstance.isEmpty()).toBeFalsy();
  });
  it("can peek the front element", function() {
    expect(queueInstance.peek()).toBe(undefined);
    queueInstance.enqueue(1);
    expect(queueInstance.peek()).toBe(1);
  });
  it("can get its own array representaion", function() {
    queueInstance.enqueue(1);
    queueInstance.enqueue(2);
    var array = queueInstance.toArray();
    expect(array[0]).toBe(1);
    expect(array[1]).toBe(2);
  });

  it("can generate log entry for every operation defined", function() {
      var QUEUE_METHOD_NAME = ConstantsModule.QUEUE_METHOD_NAME;
      var DATA_STRUCTURE_NAME = ConstantsModule.DATA_STRUCTURE_NAME;
      var QueueMethodConstants = Object.keys(QUEUE_METHOD_NAME);
      queueInstance.add(1);
      queueInstance.clear();
      queueInstance.contains(1);
      queueInstance.dequeue();
      queueInstance.enqueue(1);
      var queueInstance2 = new Queue401();
      queueInstance2.enqueue(1);
      queueInstance.equals(queueInstance2);
      queueInstance.isEmpty();
      queueInstance.peek();
      queueInstance.size();
      queueInstance.toArray();

      var log = LogManagerInstance.findAllOperationOfThisDataStructure(queueInstance.dataStructureId);
      var numLog = log.length;
      expect(numLog).toBe(11);

      // TODO: get rid of this hax once we know what to do with forEach
      var offSet = 0;
      log.forEach(function(eachLog, index, listOfLog) {
        var opName = QUEUE_METHOD_NAME[QueueMethodConstants[index + offSet]];

        if (opName === 'forEach') {
          offSet++;
          opName = QUEUE_METHOD_NAME[QueueMethodConstants[index + offSet]];
        }
        expect(eachLog.getOperationName()).toBe(opName);
      });

      // Inspect each entry
      var newEntry = log[0];
      expect(newEntry.getParamObject().length).toBe(0);
      expect(newEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var addEntry = log[1];
      expect(addEntry.getParamObject().length).toBe(1);
      expect(addEntry.getParamObject()[0]).toBe(1);
      expect(addEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var clearEntry = log[2];
      expect(clearEntry.getParamObject().length).toBe(0);
      expect(clearEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var containsEntry = log[3];
      expect(containsEntry.getParamObject().length).toBe(2);
      expect(containsEntry.getParamObject()[0]).toBe(1);
      expect(containsEntry.getParamObject()[1]).toBe(undefined);
      expect(containsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var dequeueEntry = log[4];
      expect(dequeueEntry.getParamObject().length).toBe(0);
      expect(dequeueEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var enqueueEntry = log[5];
      expect(enqueueEntry.getParamObject().length).toBe(1);
      expect(enqueueEntry.getParamObject()[0]).toBe(1);
      expect(enqueueEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var equalsEntry = log[6];
      expect(equalsEntry.getParamObject().length).toBe(2);
      expect(equalsEntry.getParamObject()[0]).toBe(queueInstance2);
      expect(equalsEntry.getParamObject()[1]).toBe(undefined);
      expect(equalsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var emptyEntry = log[7];
      expect(emptyEntry.getParamObject().length).toBe(0);
      expect(emptyEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var peekEntry = log[8];
      expect(peekEntry.getParamObject().length).toBe(0);
      expect(peekEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var sizeEntry = log[9];
      expect(sizeEntry.getParamObject().length).toBe(0);
      expect(sizeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);

      var toArrayEntry = log[10];
      expect(toArrayEntry.getParamObject().length).toBe(0);
      expect(toArrayEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.QUEUE_401);
  });

});
