describe("Graph401", function() {
  var Graph401;
  var graphInstance;
  var LogManagerInstance;
  var ConstantsModule;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../Graph401", "../../../Utility/Constants"], function(LogManagerInstanceModule, Graph401Module, Constants) {
        Graph401 = Graph401Module.Graph401;
        LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
        ConstantsModule = Constants;
        done();
    });
  });

  beforeEach(function() {
    graphInstance = new Graph401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });

});
