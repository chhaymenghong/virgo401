describe("Set401", function() {
  var Set401;
  var LogManagerInstance;
  var setInstance;
  var ConstantsModule;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../Set401", "../../../Utility/Constants"], function(LogManagerInstanceModule, Set401Module, Constants) {
      LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
      Set401 = Set401Module.Set401;
      ConstantsModule = Constants;
      done();
    });
  });
  beforeEach(function() {
    setInstance = new Set401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });
  it("should be empty when first created", function() {
    expect(setInstance.size()).toBe(0);
  });
  it("can add an item", function() {
    var addRes = setInstance.add(2);
    expect(addRes).toBeTruthy();
    addRes = setInstance.add(2);
    expect(addRes).toBeFalsy();
    expect(setInstance.size()).toBe(1);
  });
  it("can clear all items", function() {
    setInstance.add(2);
    expect(setInstance.size()).toBe(1);
    setInstance.add(3);
    expect(setInstance.size()).toBe(2);
    setInstance.clear();
    expect(setInstance.size()).toBe(0);
  });
  it("can check if it contains an items", function() {
    setInstance.add(2);
    expect(setInstance.contains(2)).toBeTruthy();
    expect(setInstance.contains(3)).toBeFalsy();
  });
  it("can do set difference", function() {
    setInstance.add(1);
    setInstance.add(2);
    var setInstance2 = new Set401();
    setInstance2.add(1);
    setInstance.difference(setInstance2);
    expect(setInstance.size()).toBe(1);
    setInstance.forEach(function(element) {
      expect(element).toBe(2);
    });
  });
  it("can check if it is equivalent to another set", function() {
    setInstance.add(1);
    setInstance.add(3);
    var setInstance2 = new Set401();
    setInstance2.add(1);
    setInstance2.add(3);
    expect(setInstance.equals(setInstance2)).toBeTruthy();
  });
  it("can do set intersection", function() {
    setInstance.add(1);
    setInstance.add(2);
    var setInstance2 = new Set401();
    setInstance2.add(1);
    setInstance.intersection(setInstance2);
    expect(setInstance.size()).toBe(1);
    setInstance.forEach(function(element) {
      expect(element).toBe(1);
    });
  });
  it("can check if it is empty", function() {
    expect(setInstance.isEmpty()).toBeTruthy();
    setInstance.add(1);
    expect(setInstance.isEmpty()).toBeFalsy();
  });
  it("can check if it is a subset of another set", function() {
    setInstance.add(1);
    setInstance.add(2);
    var setInstance2 = new Set401();
    setInstance2.add(1);
    setInstance2.add(2);
    setInstance2.add(3);
    expect(setInstance.isSubsetOf(setInstance2)).toBeTruthy();
  });
  it("can remove a specified item", function() {
    setInstance.add(1);
    expect(setInstance.size()).toBe(1);
    var remove = setInstance.remove(1);
    expect(remove).toBeTruthy();
    expect(setInstance.size()).toBe(0);
  });
  it("can get its own array representaion", function() {
    setInstance.add(1);
    setInstance.add(2);
    var array = setInstance.toArray();
    expect(array[0]).toBe(1);
    expect(array[1]).toBe(2);
  });
  it("can do set union", function() {
    setInstance.add(1);
    setInstance.add(2);
    var setInstance2 = new Set401();
    setInstance2.add(1);
    setInstance2.add(2);
    setInstance.union(setInstance2);
    expect(setInstance.size()).toBe(2);
    setInstance2.add(3);
    setInstance.union(setInstance2);
    expect(setInstance.size()).toBe(3);
    var count = 1;
    setInstance.forEach(function(element) {
      expect(element).toBe(count);
      count++;
    });
  });

  it("can generate log entry for every operation defined", function() {

      var SET_METHOD_NAME = ConstantsModule.SET_METHOD_NAME;
      var DATA_STRUCTURE_NAME = ConstantsModule.DATA_STRUCTURE_NAME;
      var SetMethodConstants = Object.keys(SET_METHOD_NAME);

      setInstance.add(2);
      setInstance.clear();
      setInstance.contains(2);
      var setInstance2 = new Set401();
      setInstance2.add(2);
      setInstance.difference(setInstance2);
      setInstance.equals(setInstance2);
      setInstance.intersection(setInstance2);
      setInstance.isEmpty();
      setInstance.isSubsetOf(setInstance2);
      setInstance.remove(2);
      setInstance.size();
      setInstance.toArray();
      setInstance.union(setInstance2);

      var log = LogManagerInstance.findAllOperationOfThisDataStructure(setInstance.dataStructureId);
      var numLog = log.length;
      expect(numLog).toBe(13);

      // check operation name

      // TODO: get rid of this hax once we know what to do with forEach
      var offSet = 0;
      log.forEach(function(eachLog, index, listOfLog) {
        var opName = SET_METHOD_NAME[SetMethodConstants[index + offSet]];

        if (opName === 'forEach') {
          offSet++;
          opName = SET_METHOD_NAME[SetMethodConstants[index + offSet]];
        }
        expect(eachLog.getOperationName()).toBe(opName);
      });
      var newEntry = log[0];
      expect(newEntry.getParamObject().length).toBe(0);
      expect(newEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var addEntry = log[1];
      expect(addEntry.getParamObject().length).toBe(1);
      expect(addEntry.getParamObject()[0]).toBe(2);
      expect(addEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var clearEntry = log[2];
      expect(clearEntry.getParamObject().length).toBe(0);
      expect(clearEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);
      var containsEntry = log[3];
      expect(containsEntry.getParamObject().length).toBe(1);
      expect(containsEntry.getParamObject()[0]).toBe(2);
      expect(addEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var differenceEntry = log[4];
      expect(differenceEntry.getParamObject().length).toBe(1);
      expect(differenceEntry.getParamObject()[0]).toBe(setInstance2);
      expect(differenceEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var equalsEntry = log[5];
      expect(equalsEntry.getParamObject().length).toBe(1);
      expect(equalsEntry.getParamObject()[0]).toBe(setInstance2);
      expect(equalsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var intersectionEntry = log[6];
      expect(intersectionEntry.getParamObject().length).toBe(1);
      expect(intersectionEntry.getParamObject()[0]).toBe(setInstance2);
      expect(intersectionEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var isEmptyEntry = log[7];
      expect(isEmptyEntry.getParamObject().length).toBe(0);
      expect(isEmptyEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var isSubsetOfEntry = log[8];
      expect(isSubsetOfEntry.getParamObject().length).toBe(1);
      expect(isSubsetOfEntry.getParamObject()[0]).toBe(setInstance2);
      expect(isSubsetOfEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var removeEntry = log[9];
      expect(removeEntry.getParamObject().length).toBe(1);
      expect(removeEntry.getParamObject()[0]).toBe(2);
      expect(removeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var sizeEntry = log[10];
      expect(sizeEntry.getParamObject().length).toBe(0);
      expect(sizeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var toArrayEntry = log[11];
      expect(toArrayEntry.getParamObject().length).toBe(0);
      expect(toArrayEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);

      var unionEntry = log[12];
      expect(unionEntry.getParamObject().length).toBe(1);
      expect(unionEntry.getParamObject()[0]).toBe(setInstance2);
      expect(unionEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.SET_401);
  });
});
