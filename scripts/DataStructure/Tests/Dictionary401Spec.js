describe("Dictionary401", function() {
  var Dictionary401;
  var LogManagerInstance;
  var dictionaryInstance;
  var ConstantsModule;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../Dictionary401", "../../../Utility/Constants"], function(LogManagerInstanceModule, Dictionary401Module, Constants) {
      LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
      Dictionary401 = Dictionary401Module.Dictionary401;
      ConstantsModule = Constants;
      done();
    });
  });
  beforeEach(function() {
    dictionaryInstance = new Dictionary401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });
  it("should be empty when first created", function() {
    expect(dictionaryInstance.size()).toBe(0);
  });
  it("can add new key value pair", function() {
    var res = dictionaryInstance.set(0, "A");
    expect(dictionaryInstance.size()).toBe(1);
    expect(res).toBe(undefined);
    res = dictionaryInstance.set(0, "B");
    expect(res).toBe("A");
  });
  it("can check if it contains a value of a specified key", function() {
    dictionaryInstance.set(0, "A");
    expect(dictionaryInstance.containsKey(0)).toBeTruthy();
  });
  it("can check if it is equivalent to another dictionary", function() {
    dictionaryInstance.set(0, "A");
    dictionaryInstance.set(0, "B");
    var d2 = new Dictionary401();
    d2.set(0, "A");
    expect(dictionaryInstance.equals(d2)).toBeFalsy();
    d2.set(0, "B");
    expect(dictionaryInstance.equals(d2)).toBeTruthy();
  });
  it("can get a value for a specified key", function() {
    dictionaryInstance.set(0, "A");
    expect(dictionaryInstance.get(0)).toBe("A");
  });
  it("can check if it is empty", function() {
    expect(dictionaryInstance.isEmpty()).toBeTruthy();
    dictionaryInstance.set("0", "B");
    expect(dictionaryInstance.isEmpty()).toBeFalsy();
  });
  it("can get all its keys", function() {
    dictionaryInstance.set(0, "A");
    dictionaryInstance.set(1, "B");
    expect(dictionaryInstance.size()).toBe(2);
    var arrayOfKeys = dictionaryInstance.keys();
    expect(arrayOfKeys.length).toBe(2);
    expect(arrayOfKeys[0]).toBe(0);
    expect(arrayOfKeys[1]).toBe(1);
  });
  it("can remove a key-value mapping", function() {
    dictionaryInstance.set(0, "A");
    dictionaryInstance.set(1, "B");
    dictionaryInstance.remove(0);
    expect(dictionaryInstance.size()).toBe(1);
  });
  it("can set a new key-value mapping", function() {
    dictionaryInstance.set(0, "A");
    expect(dictionaryInstance.get(0)).toBe("A");
  });
  it("can get all its values", function() {
    dictionaryInstance.set(0, "A");
    dictionaryInstance.set(1, "B");
    var array = dictionaryInstance.values();
    expect(array[0]).toBe("A");
    expect(array[1]).toBe("B");
  });
  it("can generate log entry for every operation defined", function() {
    var DICTIONARY_METHOD_NAME = ConstantsModule.DICTIONARY_METHOD_NAME;
    var DATA_STRUCTURE_NAME = ConstantsModule.DATA_STRUCTURE_NAME;
    var DictionaryMethodConstants = Object.keys(DICTIONARY_METHOD_NAME);

    dictionaryInstance.clear();
    dictionaryInstance.containsKey(1);
    var dict2 = new Dictionary401();
    dictionaryInstance.equals(dict2);
    dictionaryInstance.get(1);
    dictionaryInstance.isEmpty();
    dictionaryInstance.keys();
    dictionaryInstance.remove(2);
    dictionaryInstance.set(1, "A");
    dictionaryInstance.size();
    dictionaryInstance.values();


    var log = LogManagerInstance.findAllOperationOfThisDataStructure(dictionaryInstance.dataStructureId);
    var numLog = log.length;
    expect(numLog).toBe(11);

    // check operation name

    // TODO: get rid of this hax once we know what to do with forEach
    var offSet = 0;
    log.forEach(function(eachLog, index, listOfLog) {
      var opName = DICTIONARY_METHOD_NAME[DictionaryMethodConstants[index + offSet]];

      if (opName === 'forEach') {
        offSet++;
        opName = DICTIONARY_METHOD_NAME[DictionaryMethodConstants[index + offSet]];
      }
      expect(eachLog.getOperationName()).toBe(opName);
    });

    var newEntry = log[0];
    expect(newEntry.getParamObject().length).toBe(0);
    expect(newEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var clearEntry = log[1];
    expect(clearEntry.getParamObject().length).toBe(0);
    expect(clearEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var containsKeyEntry = log[2];
    expect(containsKeyEntry.getParamObject().length).toBe(1);
    expect(containsKeyEntry.getParamObject()[0]).toBe(1);
    expect(containsKeyEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var equalsEntry = log[3];
    expect(equalsEntry.getParamObject().length).toBe(2);
    expect(equalsEntry.getParamObject()[0]).toBe(dict2);
    expect(equalsEntry.getParamObject()[1]).toBe(undefined);
    expect(equalsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var getEntry = log[4];
    expect(getEntry.getParamObject().length).toBe(1);
    expect(getEntry.getParamObject()[0]).toBe(1);
    expect(getEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var isEmptyEntry = log[5];
    expect(isEmptyEntry.getParamObject().length).toBe(0);
    expect(isEmptyEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var keysEntry = log[6];
    expect(keysEntry.getParamObject().length).toBe(0);
    expect(keysEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var removeEntry = log[7];
    expect(removeEntry.getParamObject().length).toBe(1);
    expect(removeEntry.getParamObject()[0]).toBe(2);
    expect(removeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var setEntry = log[8];
    expect(setEntry.getParamObject().length).toBe(2);
    expect(setEntry.getParamObject()[0]).toBe(1);
    expect(setEntry.getParamObject()[1]).toBe("A");
    expect(setEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var sizeEntry = log[9];
    expect(sizeEntry.getParamObject().length).toBe(0);
    expect(sizeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);

    var valuesEntry = log[10];
    expect(valuesEntry.getParamObject().length).toBe(0);
    expect(valuesEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.DICTIONARY_401);


  });

});
