//**Note the path in require starts from the jasmineFramework directory
describe("Stack401", function() {
  var Stack401;
  var stackInstance;
  var LogManagerInstance;
  var ConstantsModule;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../Stack401", "../../../Utility/Constants"], function(LogManagerInstanceModule, Stack401Module, Constants) {
        Stack401 = Stack401Module.Stack401;
        LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
        ConstantsModule = Constants;
        done();
    });
  });

  beforeEach(function() {
    stackInstance = new Stack401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });

  it("should be empty when first created", function() {
    expect(stackInstance.size()).toBe(0);
  });
  it("can push items", function() {
    var pushSuccess = stackInstance.push(1);
    expect(stackInstance.size()).toBe(1);
    expect(pushSuccess).toBe(true);
    pushSuccess = stackInstance.push(2);
    expect(stackInstance.size()).toBe(2);
    expect(pushSuccess).toBe(true);
  });
  it("can add items", function() {
    var pushSuccess = stackInstance.add(1);
    expect(stackInstance.size()).toBe(1);
    expect(pushSuccess).toBe(true);
    pushSuccess = stackInstance.add(2);
    expect(stackInstance.size()).toBe(2);
    expect(pushSuccess).toBe(true);
  });
  it("can pop items", function() {
    var pushSuccess = stackInstance.push(1);
    var popItem = stackInstance.pop();
    expect(stackInstance.size()).toBe(0);
    expect(popItem).toBe(1);
    pushSuccess = stackInstance.push(200);
    pushSuccess = stackInstance.push(300);
    expect(stackInstance.size()).toBe(2);
    popItem = stackInstance.pop();
    expect(popItem).toBe(300);
    popItem = stackInstance.pop();
    expect(popItem).toBe(200);
  });
  it("can peek the top element", function() {
    var pushSuccess = stackInstance.push(120);
    var peekResult = stackInstance.peek();
    expect(stackInstance.size()).toBe(1);
    expect(peekResult).toBe(120);
    stackInstance.push(200);
    expect(stackInstance.size()).toBe(2);
    expect(stackInstance.peek()).toBe(200);
  });
  it("can check whether it contains a specified element", function() {
    stackInstance.push(120);
    var contain = stackInstance.contains(120);
    expect(contain).toBe(true);
    contain = stackInstance.contains(130);
    expect(contain).toBe(false);
    stackInstance.push(1234);
    contain = stackInstance.contains(1234);
    expect(contain).toBe(true);
  });
  it("can clear all its elements", function(){
    stackInstance.push(1);
    stackInstance.push(2);
    stackInstance.push(3);
    stackInstance.push(4);
    expect(stackInstance.size()).toBe(4);
    stackInstance.clear();
    expect(stackInstance.size()).toBe(0);
  });
  it("can check if it is empty", function() {
    stackInstance.push(1);
    expect(stackInstance.isEmpty()).toBe(false);
    stackInstance.pop();
    expect(stackInstance.isEmpty()).toBe(true);
  });
  it("can check if it is equivalent to another stack", function() {
    stackInstance.push(1);
    stackInstance.push(2);
    var stackInstance2 = new Stack401();
    stackInstance2.push(1);
    stackInstance2.push(2);

    expect(stackInstance.equals(stackInstance2)).toBeTruthy();
    stackInstance2.push(3);
    expect(stackInstance.equals(stackInstance2)).toBeFalsy();
  });
  it("can get its own array representation", function() {
    stackInstance.push(1);
    stackInstance.push(2);
    var arrayRepresentation = stackInstance.toArray();
    expect(arrayRepresentation[0]).toBe(2);
    expect(arrayRepresentation[1]).toBe(1);
  });

  it("can generate log entry for every operation defined", function() {
      var STACK_METHOD_NAME = ConstantsModule.STACK_METHOD_NAME;
      var DATA_STRUCTURE_NAME = ConstantsModule.DATA_STRUCTURE_NAME;
      var StackMethodConstants = Object.keys(STACK_METHOD_NAME);
      stackInstance.add(1);
      stackInstance.clear();
      stackInstance.contains(2);
      var stackInstance2 = new Stack401();
      stackInstance2.push(2);
      stackInstance.equals(stackInstance2);
      stackInstance.forEach(function(element) {});
      stackInstance.isEmpty();
      stackInstance.peek();
      stackInstance.pop();
      stackInstance.push(2);
      stackInstance.size();
      stackInstance.toArray();

      var log = LogManagerInstance.findAllOperationOfThisDataStructure(stackInstance.dataStructureId);
      var numLog = log.length;
      expect(numLog).toBe(11);

      // check operation name

      // TODO: get rid of this hax once we know what to do with forEach
      var offSet = 0;
      log.forEach(function(eachLog, index, listOfLog) {
        var opName = STACK_METHOD_NAME[StackMethodConstants[index + offSet]];

        if (opName === 'forEach') {
          offSet++;
          opName = STACK_METHOD_NAME[StackMethodConstants[index + offSet]];
        }
        expect(eachLog.getOperationName()).toBe(opName);
      });

      // Inspect each entry
      var newEntry = log[0];
      expect(newEntry.getParamObject().length).toBe(0);
      expect(newEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var addEntry = log[1];
      expect(addEntry.getParamObject().length).toBe(1);
      expect(addEntry.getParamObject()[0]).toBe(1);
      expect(addEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var clearEntry = log[2];
      expect(clearEntry.getParamObject().length).toBe(0);
      expect(clearEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var containsEntry = log[3];
      expect(containsEntry.getParamObject().length).toBe(2); // include optionalEqualFunction
      expect(containsEntry.getParamObject()[0]).toBe(2);
      expect(containsEntry.getParamObject()[1]).toBe(undefined);
      expect(containsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var equalsEntry = log[4];
      expect(equalsEntry.getParamObject().length).toBe(2);
      expect(equalsEntry.getParamObject()[0]).toBe(stackInstance2);
      expect(equalsEntry.getParamObject()[1]).toBe(undefined);
      expect(equalsEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      // forEach

      var isEmptyEntry = log[5];
      expect(isEmptyEntry.getParamObject().length).toBe(0);
      expect(isEmptyEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var peekEntry = log[6];
      expect(peekEntry.getParamObject().length).toBe(0);
      expect(peekEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var popEntry = log[7];
      expect(popEntry.getParamObject().length).toBe(0);
      expect(popEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var pushEntry = log[8];
      expect(pushEntry.getParamObject().length).toBe(1);
      expect(pushEntry.getParamObject()[0]).toBe(2);
      expect(pushEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var sizeEntry = log[9];
      expect(sizeEntry.getParamObject().length).toBe(0);
      expect(sizeEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);

      var toArrayEntry = log[10];
      expect(toArrayEntry.getParamObject().length).toBe(0);
      expect(toArrayEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.STACK_401);
  });
});
