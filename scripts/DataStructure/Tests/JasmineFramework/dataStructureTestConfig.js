// Config file defining paths to files
requirejs.config({
    "baseUrl": "DataStructure",
    "paths": {
      "app": "./Tests/SpecRunner", // our main entry point(html file that load index.js)
      "Array401": "Array401",
      "LogManager": "../../../Log/LogManager",
      "Constants": "../Utility/Constants",
      "Array401Spec": "./Tests/Array401Spec"

    }
});
// debugger;
requirejs(["../../Array401Spec"]); // our main entry point(js file)
