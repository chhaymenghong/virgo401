//**Note the path in require starts from the jasmineFramework directory
describe("Array401", function() {
  var Array401;
  var arrayInstance;
  var LogManagerInstance;
  beforeAll(function(done) {
    require(["../../../Log/LogManager", "../../Array401"], function(LogManagerInstanceModule, Array401Module) {
        Array401 = Array401Module.Array401;
        LogManagerInstance = LogManagerInstanceModule.LogManagerInstance;
        done();
    });
  });
  beforeEach(function() {
    arrayInstance = new Array401();
  });
  afterEach(function(done) {
    require(["../../../Log/LogManager"], function(LogManagerInstanceModule) {
      LogManagerInstance.clearAllEntries();
      done();
    });
  });

  it("should be empty when first created", function() {
    expect(arrayInstance.length).toBe(0);
  });
  it("can push items", function() {
    var res = arrayInstance.push(1000);
    expect(arrayInstance.length).toBe(1);
    expect(res).toBe(1);
    var res2 = arrayInstance.push(2000);
    expect(arrayInstance.length).toBe(2);
    expect(res2).toBe(2);
    expect(arrayInstance[0]).toBe(1000);
    expect(arrayInstance[1]).toBe(2000);

  });
  it("can pop items", function() {
    var pushRes = arrayInstance.push(1);
    var popRes = arrayInstance.pop();
    expect(popRes).toBe(1);
    expect(arrayInstance.length).toBe(0);
    popRes = arrayInstance.pop();
    expect(popRes).toBe(undefined);
    arrayInstance.push(1000);
    arrayInstance.push(2000);
    expect(arrayInstance.length).toBe(2);
    arrayInstance.pop();
    arrayInstance.pop();
    expect(arrayInstance.length).toBe(0);
  });

  it("can find indexOf an item", function() {
    arrayInstance.push(1000);
    var find = arrayInstance.indexOf(1000);
    expect(find).toBe(0);
    arrayInstance.push(2000);
    find = arrayInstance.indexOf(2000);
    expect(find).toBe(1);
    arrayInstance.pop();
    find = arrayInstance.indexOf(2000);
    expect(find).toBe(-1);
  });

  it("can insert at a specified index", function() {
    arrayInstance.insertAtIndex(0, 1000);
    expect(arrayInstance.length).toBe(1);
    var resPop = arrayInstance.pop();
    expect(resPop).toBe(1000);
    arrayInstance.insertAtIndex(0, 2000);
    arrayInstance.insertAtIndex(1, 3000);
    expect(arrayInstance.length).toBe(2);
    resPop = arrayInstance.pop();
    expect(resPop).toBe(3000);
    resPop = arrayInstance.pop();
    expect(resPop).toBe(2000);
    expect(arrayInstance.length).toBe(0);
  });
  it("can replace an element at a specified index", function() {
    arrayInstance.push(1000);
    arrayInstance.replaceAtIndex(0, 2000);
    var popRes = arrayInstance.pop();
    expect(popRes).toBe(2000);
  });
  it("can generate log entry for every operation defined", function() {
    require(["../../../Utility/Constants"], function(ConstantsModule) {

      var ARRAY_METHOD_NAME = ConstantsModule.ARRAY_METHOD_NAME;
      var DATA_STRUCTURE_NAME = ConstantsModule.DATA_STRUCTURE_NAME;
      var ArrayMethodConstants = Object.keys(ARRAY_METHOD_NAME);
      arrayInstance.push(5);
      arrayInstance.pop();
      arrayInstance.indexOf(0);
      arrayInstance.insertAtIndex(0, 1);
      arrayInstance.replaceAtIndex(0, 1000);
      arrayInstance.selectIndex(0);
      // Check operation name
      var log = LogManagerInstance.findAllOperationOfThisDataStructure(arrayInstance.dataStructureId);
      var numLog = log.length;
      expect(numLog).toBe(7);
      log.forEach(function(eachLog, index, listOfLog) {
        expect(eachLog.getOperationName()).toBe(ARRAY_METHOD_NAME[ArrayMethodConstants[index]]);
      });
      // Inspect each entry
      var newEntry = log[0]
      expect(newEntry.getParamObject().length).toBe(0);
      expect(newEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.ARRAY_401);

      var pushEntry = log[1];
      expect(pushEntry.getParamObject().length).toBe(1);
      expect(pushEntry.getParamObject()[0]).toBe(5);
      expect(pushEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.ARRAY_401);

      var popEntry = log[2];
      expect(popEntry.getParamObject().length).toBe(0);
      expect(popEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.ARRAY_401);

      var indexOfEntry = log[3];
      expect(indexOfEntry.getParamObject().length).toBe(1);
      expect(indexOfEntry.getParamObject()[0]).toBe(0);
      expect(indexOfEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.ARRAY_401);

      var insertAtIndexEntry = log[4];
      expect(insertAtIndexEntry.getParamObject().length).toBe(2);
      expect(insertAtIndexEntry.getParamObject()[0]).toBe(0);
      expect(insertAtIndexEntry.getParamObject()[1]).toBe(1);
      expect(insertAtIndexEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.ARRAY_401);

      var replaceAtIndexEntry = log[5];
      expect(replaceAtIndexEntry.getParamObject().length).toBe(2);
      expect(replaceAtIndexEntry.getParamObject()[0]).toBe(0);
      expect(replaceAtIndexEntry.getParamObject()[1]).toBe(1000);
      expect(replaceAtIndexEntry.getDataStructureType()).toBe(DATA_STRUCTURE_NAME.ARRAY_401);
    });
  });
});
