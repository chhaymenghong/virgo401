define(["../Library/graph", "../Utility/Constants", "../Log/LogManager"], function(Graph401Module, Constants, LogManagerModule) {
  var logManagerInstance;
  function Graph401() {
    this.innerGraph = new Graph401Module.Graph();
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.NEW, {}, this.dataStructureId);
  }
  Graph401.prototype.constructor = Graph401;

  Graph401.prototype.addNode = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.ADD_NODE, {}, this.dataStructureId, node);
    this.innerGraph.addNode(node);
    return node;
  };
  Graph401.prototype.removeNode = function(node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.REMOVE_NODE, {}, this.dataStructureId, node);
    this.innerGraph.removeNode(node);
    return node;
  };
  Graph401.prototype.getNode = function (label) {
    var node = this.innerGraph.getNodeWithLabel(label);
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.GET_NODE, {}, this.dataStructureId, node);
    return node;
  };
  Graph401.prototype.getNodes = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.GET_NODES, {}, this.dataStructureId);
    return this.innerGraph.getAllNodes();
  };
  Graph401.prototype.setEdge = function(pNode1, pNode2, pNode3) {
    var setResult = this.innerGraph.setEdgeD(pNode1, pNode2, pNode3);
    var n1 = this.innerGraph.getNodeWithLabel(pNode1.label);
    var n2 = this.innerGraph.getNodeWithLabel(pNode2.label);
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.SET_EDGE_D, {node1: n1, node2: n2 }, this.dataStructureId, pNode3);
    return setResult;
  };
  Graph401.prototype.removeEdge = function (pNode1, pNode2) {
    var removeEdgeNode = this.innerGraph.removeEdge(pNode1, pNode2);
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.REMOVE_EDGE, {node1: pNode1, node2: pNode2 }, this.dataStructureId, removeEdgeNode);
  };
  Graph401.prototype.neighbors = function (node) {
    var pNeighbors = this.innerGraph.getNeighbor(node);
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.NEIGHBORS, {neighbors: pNeighbors}, this.dataStructureId, node);
    return pNeighbors;
  };
  Graph401.prototype.getEdge = function (pNode1, pNode2) {
    if (!pNode1 || !pNode2) throw "Invalid Argument";
    var node3 = this.innerGraph.getEdge(pNode1, pNode2);
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.GRAPH_401, Constants.GRAPH_METHOD_NAME.GET_EDGE, {node1: pNode1, node2: pNode2}, this.dataStructureId, node3);
    return node3;
  };

  return {
    Graph401 : Graph401
  };
});
