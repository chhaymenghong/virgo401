// Require js makes
define(["../Library/buckets", "../Utility/Constants", "../Log/LogManager"], function(buckets, Constants, LogManagerModule) {
  var logManagerInstance;
  function BinarySearchTree401(compareFunction) {
    this.innerBinarySearchTree = new buckets.BSTree(compareFunction);
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.NEW, [], this.dataStructureId);
  }
  BinarySearchTree401.prototype.constructor = BinarySearchTree401;

  BinarySearchTree401.prototype.add = function(value) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.ADD, [value], this.dataStructureId);
    return this.innerBinarySearchTree.add(value);
  };
  BinarySearchTree401.prototype.clear = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.CLEAR, [], this.dataStructureId);
    return this.innerBinarySearchTree.clear();
  };
  BinarySearchTree401.prototype.contains = function(value) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.CONTAINS, [value], this.dataStructureId);
    return this.innerBinarySearchTree.contains(value);
  };

  BinarySearchTree401.prototype.equals = function(other) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.EQUALS, [other], this.dataStructureId);
    return this.innerBinarySearchTree.equals(other);
  };
  BinarySearchTree401.prototype.forEach = function(callback) {
    this.innerBinarySearchTree.forEach(callback);
  };
  BinarySearchTree401.prototype.height = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.HEIGHT, [], this.dataStructureId);
    this.innerBinarySearchTree.height();
  };
  BinarySearchTree401.prototype.inorderTraversal = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.INORDER_TRAVERSAL, [], this.dataStructureId);
    return this.innerBinarySearchTree.inorderTraversal();
  };
  BinarySearchTree401.prototype.isEmpty = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.IS_EMPTY, [], this.dataStructureId);
    return this.innerBinarySearchTree.isEmpty();
  };
  BinarySearchTree401.prototype.levelTraversal = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.LEVEL_TRAVERSAL, [], this.dataStructureId);
    return this.innerBinarySearchTree.levelTraversal();
  };
  BinarySearchTree401.prototype.maximum = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.MAXIMUM, [], this.dataStructureId);
    return this.innerBinarySearchTree.maximum();
  };
  BinarySearchTree401.prototype.minimum = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.MINIMUM, [], this.dataStructureId);
    return this.innerBinarySearchTree.minimum();
  };
  BinarySearchTree401.prototype.postorderTraversal = function(callback) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.POSTORDER_TRAVERSAL, [callback], this.dataStructureId);
    this.innerBinarySearchTree.postorderTraversal(callback);
  };
  BinarySearchTree401.prototype.preorderTraversal = function(callback) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.PREORDER_TRAVERSAL, [callback], this.dataStructureId);
    this.innerBinarySearchTree.preorderTraversal(callback);
  };
  BinarySearchTree401.prototype.remove = function(value) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.REMOVE, [value], this.dataStructureId);
    this.innerBinarySearchTree.remove(value);
  };
  BinarySearchTree401.prototype.size = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.SIZE, [], this.dataStructureId);
    return this.innerBinarySearchTree.size();
  };
  BinarySearchTree401.prototype.toArray = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.TO_ARRAY, [], this.dataStructureId);
    return this.innerBinarySearchTree.toArray();
  };
  BinarySearchTree401.prototype.selectAtValue = function(value) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.BINARY_SEARCH_TREE_401, Constants.BINARY_SEARCH_TREE_METHOD_NAME.SELECT_AT_VALUE, [value], this.dataStructureId);
  };
  //
  // BinarySearchTree401.prototype.getListOfEntry = function() {
  //   return logManagerInstance.getListOfEntry();
  // };

  return {
    BinarySearchTree401 : BinarySearchTree401
  };
});
