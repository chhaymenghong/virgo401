define(["../Library/buckets", "../Utility/Constants", "../Log/LogManager"], function(buckets, Constants, LogManagerModule) {
  var logManagerInstance;
  function LinkedList401() {
    this.innerLinkedList = new buckets.LinkedList();
    logManagerInstance = LogManagerModule.LogManagerInstance;
    this.dataStructureId = logManagerInstance.getNewIdForNewDataStructure();
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.LINKED_LIST_401, Constants.LINKED_LIST_METHOD_NAME.NEW, {}, this.dataStructureId);
  }
  LinkedList401.prototype.constructor = LinkedList401;
  LinkedList401.prototype.length = function() {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.LINKED_LIST_401, Constants.LINKED_LIST_METHOD_NAME.LENGTH, {}, this.dataStructureId);
    return this.innerLinkedList.size();
  };
  LinkedList401.prototype.insertAt = function(pIndex, node) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.LINKED_LIST_401, Constants.LINKED_LIST_METHOD_NAME.INSERT_AT, {index: pIndex}, this.dataStructureId, node);
    return this.innerLinkedList.add(node, pIndex);
  };
  LinkedList401.prototype.removeAt = function(pIndex) {
    logManagerInstance.addNewLogEntry(Constants.DATA_STRUCTURE_NAME.LINKED_LIST_401, Constants.LINKED_LIST_METHOD_NAME.REMOVE_AT, {index: pIndex}, this.dataStructureId);
    return this.innerLinkedList.removeElementAtIndex(pIndex);
  };

  return {
    LinkedList401 : LinkedList401
  };
});
