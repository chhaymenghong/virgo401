require(["jquery",
		"interpreter",
		"./DataStructure/Virgo401",
		"d3",
		"codemirror",
		"./Editor/javascript",
		"./Editor/matchbrackets",
		"scripts/Editor/comment.js",
		"scripts/Editor/continuecomment.js",
		"bootstrap"
	], function($, Interpreter, Virgo401Module, d3, CodeMirror) {
	// Initialize the JS Editor
	var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
		lineWrapping:true,
		mode: "javascript",
		lineNumbers: true,
		matchBrackets: true,
		styleActiveLine: true,
		continueComments: "Enter",
		extraKeys: {
			"Ctrl-/": "toggleComment",
			"Ctrl-S": SaveCode
		},
		theme: 'monokai'
	});
	// Our DataStructure Library
	var Virgo401 = Virgo401Module.Virgo401;

	// Hook up the button to eval the JS code
	$("#cv-go").click(GenerateVisualization);
	$("#cv-save").click(SaveCode);
	$("#cv-load").click(LoadCode);
	$("#cv-toggle").click(ToggleConsole);
	$("#cv-data").click(function() {
		ToggleReference(false);
	});
	$("#cv-api").click(function() {
		ToggleReference(true);
	});

	$("#visualizer").height($('body').height() - 250);
	LoadCode();

	window.resize = function(event, continuation) {
		$("#visualizer").height($('body').height() - 250);
		if (continuation) continuation();
	};

	$(window).resize(window.resize);

	var sample_code = {};
	$.when(
		// $.get("/samples/sample.js", function(code) {
		// 	sample_code['sample.js'] = code;
		// }, "text"),
		$.get("samples/graph_basic.js", function(code) {
			sample_code['graph_basic.js'] = code;
		}, "text"),
		// $.get("/samples/stack_basic.js", function(code) {
		// 	sample_code['stack_basic.js'] = code;
		// }, "text"),
		$.get("samples/depth_first_search.js", function(code) {
			sample_code['depth_first_search.js'] = code;
		}, "text"),
		$.get("samples/ford_fulkerson.js", function(code) {
			sample_code['ford_fulkerson.js'] = code;
		}, "text")
	).then(function() {
		var li = $("<li></li>");
		var keys = Object.keys(sample_code);
		keys.sort();
		for (var i = 0; i < keys.length; i++) {
			var file = keys[i];
			var code = sample_code[file];
			var btn = $("<button>"+file+"</button>");
			li.append(btn);
			$("#cv-samples").append(li);
			btn.click(createButton(code));
		}
	});

	function createButton(code) {
		return function() { editor.getDoc().setValue(code); };
	}

	var displayConsole = true;
	function ToggleConsole() {
		var input_size = 250;
		var controls_size = 30;
		displayConsole = !displayConsole;
		if (displayConsole) {
			$("#cv-console").css('bottom', '0px');
			$("#cv-toggle i").toggleClass('fa-chevron-down', true);
			$("#cv-toggle i").toggleClass('fa-chevron-up', false);
		} else {
			$("#cv-console").css('bottom', '-250px');
			$("#cv-toggle i").toggleClass('fa-chevron-down', false);
			$("#cv-toggle i").toggleClass('fa-chevron-up', true);
		}
	}

	function ToggleReference(display) {
		if (display) {
			$("#cv-api").toggleClass('cv-selected', true);
			$("#cv-data").toggleClass('cv-selected', false);
			$("#cv-reference").css("visibility", "visible");
			$("#cv-standby").css("visibility", "hidden");
		} else {
			$("#cv-api").toggleClass('cv-selected', false);
			$("#cv-data").toggleClass('cv-selected', true);
			$("#cv-reference").css("visibility", "hidden");
			$("#cv-standby").css("visibility", "visible");
		}
	}

	function SaveCode() {
		localStorage.setItem("cv_code_storage", editor.getValue());
	}

	function LoadCode() {
		var code = localStorage.getItem("cv_code_storage");
		if (code == null) return;
		editor.getDoc().setValue(code);
	}

	var visualizer = null;
	var playInterval = null;
	var playing = false;
	function TogglePlay() {
		if (!visualizer) return;

		if (!playing) {
			playing = true;
			play();
		} else {
			playing = false;
			pause();
		}

		function play() {
			playInterval = setInterval(function() {
				if(!playing) pause();
				else if (!visualizer.Step()) stop();
			}, 1000);
			$("#cv-play i").toggleClass('fa-pause', true);
			$("#cv-play i").toggleClass('fa-play', false);
		}

		function pause() {
			clearInterval(playInterval);
			playInterval = null;
			$("#cv-play i").toggleClass('fa-pause', false);
			$("#cv-play i").toggleClass('fa-play', true);
		}

		function stop() {
			pause();
			ToggleControls(false);
		}
	}

	function ToggleControls(on) {
		if (on) {
			$("#cv-play")[0].disabled = false;
			$("#cv-next")[0].disabled = false;
		} else {
			$("#cv-play")[0].disabled = true;
			$("#cv-next")[0].disabled = true;
		}
	}

	function GenerateVisualization() {
		"use strict";
		// Eval the JS code and generate an op log
		// Note: A lot of things are exposed to the eval right now
		var log_manager = Virgo401.LogManager;
		var code = editor.getValue();
		var _v = Virgo401;
		eval(code);

		// Create a new interpreter instance for the new log
		var ops_log = log_manager.getAllOperations();
		visualizer = new Interpreter(ops_log, $("#visualizer"));

		// Enable the visualizer controls
		ToggleControls(true);
		$("#cv-play").unbind("click");
		$("#cv-play").click(TogglePlay);
		$("#cv-next").unbind("click");
		$("#cv-next").click(function() {
			if (!visualizer.Step()) {
				ToggleControls(false);
			}
		});

		$(window).unbind("resize");
		$(window).resize(function() {
			window.resize(null, visualizer.Resize)
		});

		// Enable saving
		// $("#cv-save")[0].disabled = false;

		// clear the logEntries
		log_manager.clearAllEntries();
	}
});
