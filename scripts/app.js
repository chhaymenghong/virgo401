// Config file defining paths to files
requirejs.config({
    "baseUrl": "scripts",
    shim : {
        "bootstrap" : { "deps" :['jquery'] }
    },
    "paths": {
      "app": "../index_v2", // our main entry point(html file that load index.js)
      "jquery": "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery",
      "bootstrap": "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap",
      "d3": "https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.13/d3",
      "interact": "./Interact/interact",
      "interpreter": "./Interpreter/interpreter"
      // "virgo401Lib": "./DataStructure/Virgo401"
    },
    "packages": [{
      name: "codemirror",
      location: "./Editor",
      main: "codemirror"
    }]
});
requirejs(["index"]); // our main entry point(js file)
