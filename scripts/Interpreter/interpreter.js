define(['./Interpreter/canvas-manager', './Interpreter/graph', './Interpreter/queue', './Interpreter/stack'], function(CVManager, Graph, Queue, Stack) {
	// Operation_log is an array of log entries
	var Interpreter = function(operation_log, container) {
		var PC = 0;
		var manager = new CVManager(container);
		var displays = {};
		var node_list = {};
		var self = this;

		this.Step = function() {
			while(!evalEntry(PC++)) {}
			if (PC >= operation_log.length) {
				return false;
			} else {
				return true;
			}
		};

		this.Eval = function() {
			evalEntry(PC);
		};

		this.Resize = function() {
			manager.Resize();
			for (var id in displays) {
			    if (displays.hasOwnProperty(id)) {
			        displays[id].resize();
			    }
			}
		};

		function evalEntry(index) {
			if (index >= operation_log.length) return true;
			var entry = operation_log[index];
			entry.type = entry.dataStructureType;
			entry.op = entry.operationName;
			entry.param = entry.parameterObj;
			// console.log(entry);
			switch(entry.type) {
				case "Node401":
					switch(entry.op) {
						case "new":
							node_list[entry.node.nodeId] = entry.node;
							return false;
						case "get":
							node_list[entry.node.nodeId].accessed = true;
							break;
						case "set":
							node_list[entry.node.nodeId].modified = true;
							node_list[entry.node.nodeId].fields[entry.param.field] = entry.param.value;
							break;
						case "select":
							node_list[entry.node.nodeId].highlight = true;
							node_list[entry.node.nodeId].color = entry.param.color;
							break;
						case "deselect":
							node_list[entry.node.nodeId].dehighlight = true;
							break;
						default:
							return false;
					}
					self.Resize();
					break;
				case "Stack401":
					switch(entry.op) {
						// Stack OPs
						case "new":
							manager.NewCanvas(entry.id, entry.type);
							var d = initSVG(entry.id, entry.type);
							if (d === null) return false;
							displays[entry.id] = d;
							break;
						case "push":
							// Get the node from our already stored list so we have the augmented version.
							displays[entry.id].push(node_list[entry.node.nodeId]);
							break;
						case "pop":
							displays[entry.id].pop();
							break;
						case "peek":
							displays[entry.id].peek();
							break;
						case "isEmpty":
							// TODO:
							break;
						default:
							return false;
					}
					displays[entry.id].resize();
					break;
				case "Queue401":
					return false;
					switch(entry.op) {
						// Queue OPs
						case "new":
							manager.NewCanvas(entry.id, entry.type);
							var d = initSVG(entry.id, entry.type);
							if (d === null) return false;
							displays[entry.id] = d;
							break;
						case "enqueue":
							displays[entry.id].enqueue(node_list[entry.node.nodeId]);
							break;
						case "dequeue":
							displays[entry.id].dequeue();
							break;
						case "peek":
							displays[entry.id].peek();
							break;
						case "isEmpty":
							break;
						default:
							return false;
					}
					displays[entry.id].resize();
					break;
				case "Array401":
					switch(entry.op) {
						// Array OPs
						case "new":
							break;
						case "length":
							break;
						case "get":
							break;
						case "set":
							break;
						default:
							return false;
					}
					break;
				case "LinkedList401":
					switch(entry.op) {
						// LinkedList OPs
						case "new":
							manager.NewCanvas(entry.id, entry.type);
							var d = initSVG(entry.id, entry.type);
							if (d === null) return false;
							displays[entry.id] = d;
							break;
						case "size":
							return false;
						case "insertAt":
							break;
						case "removeAt":
							break;
						default:
							return false;
					}
					break;
				case "Graph401":
					switch(entry.op) {
						// Graph OPs
						case "new":
							manager.NewCanvas(entry.id, entry.type);
							var d = initSVG(entry.id, entry.type);
							if (d === null) return false;
							displays[entry.id] = d;
							return false;
						case "addNode":
							displays[entry.id].addNode(node_list[entry.node.nodeId]);
							break;
						case "removeNode":
							displays[entry.id].removeNode(entry.node.label);
							break;
						case "getNode":
							displays[entry.id].getNode(entry.node.label);
							break;
						case "getNodes":
							displays[entry.id].getNodes();
							break;
						case "setEdge":
							var node = node_list[entry.node.nodeId];
							if (!node) node_list[entry.node.nodeId] = {label:"",fields:{}};
							displays[entry.id].setEdge(entry.param.node1.label, entry.param.node2.label, node_list[entry.node.nodeId]);
							break;
						case "getEdge":
							displays[entry.id].getEdge(entry.param.node1.label, entry.param.node2.label);
							break;
						case "removeEdge":
							displays[entry.id].removeEdge(entry.param.node1.label, entry.param.node2.label);
							break;
						case "neighbors":
							displays[entry.id].getNeighbors(entry.node);
							break;
						default:
							return false;

					}
					displays[entry.id].resize();
					break;
				// Maps, Sets, BST if time permits
				default:
					console.log("Unknown data structure:" + entry.type);
					return false;
			}
			return true;
		}

		function initSVG(id, type) {
			var display = null;
			switch (type) {
				case "Stack401":
					manager.canvases[id].SetDimensions(400, 650);
					display = new Stack(manager.canvases[id]);
					break;
				case "Queue401":
					manager.canvases[id].SetDimensions(1000, 100);
					display = new Queue(manager.canvases[id]);
					break;
				case "Graph401":
					manager.canvases[id].SetDimensions(650, 650);
					display = new Graph(manager.canvases[id]);
					break;
			}
			return display;
		}
	};

	return Interpreter;
});
