define(function() {
	var counter = 0;
	function genUID() {
		return counter++;
	}
	
	var Graph = function(canvas) {
		var colors = d3.scale.category10();


		// var zoom = d3.behavior.zoom()
		// 	.scaleExtent([1, 5])
		// 	.on("zoom", zoomed);
		// var rect = svg.append("rect")
		//     .attr("width", canvas.width)
		//     .attr("height", canvas.height)
		//     .style("fill", "none")
		//     .style("pointer-events", "all");

		var svg = d3.select(canvas.svg[0]).attr('oncontextmenu', 'return false;');
		var defs = svg.append("svg:defs");
		var path = svg.append('svg:g').selectAll('path');
		var circle = svg.append('svg:g').selectAll('g');
		var use_tags = svg.append('svg:g');
		var labels = svg.append('svg:g');

		var nodes = [];
		var links = [];
		var lastNodeId = 0;
		var lastEdgeId = 0;

		var ratio = 0.05;
		var max_radius = 25;
		var radius = max_radius;

		// Hookup buttons
        // <div class="cv-canvas-ctrl"></div>
        // <button class="cv-canvas-btn"><i class="fa fa-search-plus"></i></button>
        // <button class="cv-canvas-btn"><i class="fa fa-search-minus"></i></button>
        // <button class="cv-canvas-btn"><i class="fa fa-unlock"></i></button>
        // <button class="cv-canvas-btn"><i class="fa fa-times"></i></button>

		var force = d3.layout.force()
		    .nodes(nodes)
		    .links(links)
		    .size([canvas.width, canvas.height])
		    .linkDistance(150)
			.charge(function() {
		    	return -0.8 * radius * radius;
		    })
		    .on('tick', tick);

		var dragging = false;
		var drag = force.drag()
		    .on("dragstart", function(d) {
	            if(d3.event.sourceEvent.button == 2) {
	            	dragging = true;
					d3.select(this).classed("cv-node-fixed", d.fixed = true);
	            } else {
	            	force.stop();
	        	}
		    })
		    .on("dragend", function() {
	            if(d3.event.sourceEvent.button == 2) {
	            	dragging = false;
	            }
		    });

		function middleClick(d) {
			if(d3.event.button != 1) return;
			d3.select(this).classed("cv-node-fixed", d.fixed = false);
		}

		function appendMarker(selection) {
			var id = genUID();
			selection.append('svg:marker')
				.attr('class', 'cv-marker')
			    .attr('id', 'arrow-'+id)
			    .attr('viewBox', '0 -5 10 10')
			    .attr('refX', 5)
			    .attr('markerWidth', 3)
			    .attr('markerHeight', 3)
			    .attr('orient', 'auto')
			  .append('svg:path')
			    .attr('d', 'M0,-5L10,0L0,5')
			    .attr('fill', '#000');
			return '#arrow-'+id;
		}

		// For solving the problem of labels being hidden
		function createUseTag(id) {
			var tag_id = "#tag-"+id;
			use_tags.append("use").attr("xlink:href", id);
			return tag_id;
		}

		function removeUseTag(tag_id) {
			$(tag_id).remove();
		}

		// update force layout (called automatically each iteration)
		function tick(e) {
			// draw directed edges with proper padding from node centers
			path.attr('d', function(d) {
				var deltaX = d.target.x - d.source.x,
				deltaY = d.target.y - d.source.y,
				dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
				normX = deltaX / dist,
				normY = deltaY / dist,
				sourcePadding = d.left ? radius + 5 : radius,
				targetPadding = d.right ? radius + 5 : radius,
				sourceX = d.source.x + (sourcePadding * normX),
				sourceY = d.source.y + (sourcePadding * normY),
				targetX = d.target.x - (targetPadding * normX),
				targetY = d.target.y - (targetPadding * normY);
				return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
			});

			circle.attr('transform', function(d) {
				return 'translate(' + d.x + ',' + d.y + ')';
			});
		}

		function dispText() {
			return radius < max_radius * 0.4 ? 0 : 1;
		}

		function linkLength(d) {
			var d_x = d.target.x - d.source.x;
			var d_y = d.target.y - d.source.y;
			return Math.sqrt(d_x * d_x + d_y * d_y);
		}

		function getFields(node) {
			if (!node.fields) return ["", ""];
			var keys = Object.keys(node.fields);
			keys.sort();
			return [node.fields[keys[0]], node.fields[keys[1]]];
		}
		
		function fieldsToString(fields) {
			var f1 = fields[0];
			var f2 = fields[1];
			string = ""
			if (f1 != null && f1 != undefined && f1 !== "") {
				string = f1;
				if (f2 != null && f2 != undefined && f2 !== "") string += ", " + f2;
			} else if (f2 != null && f2 != undefined && f2 !== "") {
				string = f2;
			}
			
			return string;
		}

		// var increment = 0.1;
		// var zoom_level = 1;
		// function zoom(scale) {
		// 	// container.attr("transform", "scale(" + scale + ")");
		// }

		// this.zoomIn = function() {
		// 	zoom_level += increment;
		// 	zoom(zoom_level)
		// }

		// this.zoomOut = function() {
		// 	zoom_level -= increment;
		// 	Math.max(zoom_level, 0.2);
		// 	zoom(zoom_level)
		// }

		this.resize = function() {
			force.charge(function() {
		    	return -0.8 * radius * radius;
		    });
			var totalVolume = canvas.width * canvas.height;
			// var ratio = nodes.length * Math.PI * r * r / totalVolume;
			radius = Math.min(Math.sqrt(ratio * totalVolume / (nodes.length * Math.PI)), max_radius);
			// path (link) group
			path = path.data(links);
			circle = circle.data(nodes, function(d) { return d.id; });

			// Update text, links, nodes
			path
				.each(function(d) {
					var self = d3.select(this);
					var label = d3.select("#lb-"+d.name);
					var text = fieldsToString(getFields(d.value));
					label.select("textPath").text(text);
					if (d.selected) {
						d.selected = false;
						self.classed('cv-edge-selected', true);
						d3.select(d.arrow).classed('cv-edge-selected', true);
						var tag_id = createUseTag("#" + d.name);
						setTimeout(function () {
							self.classed('cv-edge-selected', false);
							d3.select(d.arrow).classed('cv-edge-selected', false);
							removeUseTag(tag_id);
						}, 1000);
					}
					if (d.value.accessed) {
						d.value.accessed = false;
						label.classed('cv-label-accessed', true);
						setTimeout(function () {
							label.classed('cv-label-accessed', false);
						}, 1000);
					}
					if (d.value.modified) {
						d.value.modified = false;
						label.classed('cv-label-modified', true);
						setTimeout(function () {
							label.classed('cv-label-modified', false);
						}, 1000);
					}
					if (d.value.highlight) {
						d.value.highlight = false;
						self.style("stroke", d.value.color);
						d3.select(d.arrow).select("path").style("fill", d.value.color);
					} else if (d.value.dehighlight) {
						d.value.dehighlight = false;
						self.style("stroke", null);
						d3.select(d.arrow).select("path").style("fill", null);
					}
					if (d.removed) {
						d.removed = false;
						d.dead = true;
						self.classed('cv-edge-removed', true);
						d3.select(d.arrow).classed('cv-edge-removed', true);
						setTimeout(function () {
							self.classed('cv-edge-removed', false);
							d3.select(d.arrow).classed('cv-edge-removed', false);
							var remove = path.filter(function(d) {
								return d.dead;
							}).transition()
							.duration(300)
							.attr("stroke-width", 0);
							remove.call(function() {
								setTimeout(function () {
									links.splice(d.removed_index, 1);
									d3.select("#lb-"+d.name).remove();
								}, 300);
							});
						}, 700);
					}
				});

			circle.selectAll('circle')
				.attr('r', radius)
				.each(function(d) {
					// Update any text
					d.label.text(function(d) { return d.value.label; })
					d.field1.text(function(d) { return getFields(d.value)[0]; })
					d.field2.text(function(d) { return getFields(d.value)[1]; })
					if (d.value.accessed) {
						d.value.accessed = false;
						d.field1.classed('accessed', true);
						d.field2.classed('accessed', true);
						setTimeout(function () {
							d.field1.classed('accessed', false);
							d.field2.classed('accessed', false);
						}, 1000);
					}
					if (d.value.modified) {
						d.value.modified = false;
						d.field1.classed('modified', true);
						d.field2.classed('modified', true);
						setTimeout(function () {
							d.field1.classed('modified', false);
								d.field2.classed('modified', false);
						}, 1000);
					}

					var self = d3.select(this);
					if (d.selected) {
						d.selected = false;
						self.classed('cv-node-selected', true);
						setTimeout(function () {
							self.classed('cv-node-selected', false);
						}, 1000);
						return true;
					} 
					if (d.value.highlight) {
						d.value.highlight = false;
						self.style("stroke", d.value.color);
					} else if (d.value.dehighlight) {
						d.value.dehighlight = false;
						self.style("stroke", null);
					}
				});
			
			circle.selectAll('text')
				.style("fill-opacity", dispText);

			// Enter Links
			path.enter().append('path')
				.attr('class', 'link cv-edge-new')
				.each(function(d) {
					// Get the fields of the edge
					var fields = getFields(d.value);
					// Create an arrowhead for it
					d.arrow = appendMarker(defs);
					d3.select(d.arrow).classed('cv-edge-new', true);
					var self = d3.select(this);
					// Generate a unique id for us too
					d.name = "link-"+genUID();
					self.attr('id', d.name);
					// Create the link path text
					labels.append("text")
						.attr("id", "lb-"+d.name)
						.attr("class", "cv-label-text")
						.attr("x", 0)
						.attr("dy", -7)
						.attr("text-anchor", "middle")
						.attr("class", "cv-textpath")
						.append("textPath")
						.attr("startOffset", "50%")
						.attr('xlink:href', "#" + d.name)
						.text(fieldsToString(getFields(d.value)));
					setTimeout(function () {
						self.classed('cv-edge-new', false);
						d3.select(d.arrow).classed('cv-edge-new', false);
					}, 1000);
				})
				.style('marker-end', function(d) { return 'url('+d.arrow+')'; })
				.attr('stroke-width', 0)
				.transition()
				.duration(500)
				.attr('stroke-width', 5);

			// Enter Nodes
			var g = circle.enter().append('g').on("click", middleClick).call(drag);

			g.append('circle')
				.attr('class', 'cv-node-circle cv-node-new')
				.call(function() {
					var self = this;
					setTimeout(function () {
						self.classed('cv-node-new', false);
					}, 1000);
				})
				.attr('r', 0)
				.on('mouseover', function(d) {
					// enlarge target node
					d3.select(this).attr('transform', 'scale(1.1)');
				})
				.on('mouseout', function(d) {
					// unenlarge target node
					d3.select(this).attr('transform', '');
				})
				.transition()
				.duration(500)
				.attr('r', radius);

			// Enter Text
			g.append('text')
				.attr('x', 0)
				.attr('y', 0)
				.attr('class', 'cv-circle-label')
				.text(function(d) { d.label = d3.select(this); return d.value.label; })
				.style("fill-opacity", dispText);
			g.append('text')
				.attr('x', radius * 1.2)
				.attr('y', -10)
				.attr('class', 'cv-circle-text')
				.text(function(d) {
					d.field1 = d3.select(this); 
					return getFields(d.value)[0];
				})
				.style("fill-opacity", dispText);
			g.append('text')
				.attr('x', radius * 1.2)
				.attr('y', 10)
				.attr('class', 'cv-circle-text')
				.text(function(d) {
					d.field2 = d3.select(this); 
					return getFields(d.value)[1];
				})
				.style("fill-opacity", dispText);

			// Exit
			var exit = circle.exit();
			exit
				.selectAll("circle")
				.classed('cv-node-remove', true)
				.call(function() {
					setTimeout(function () {
						exit.selectAll("circle").transition().duration(250).attr("r", 0);
						setTimeout(function () { exit.remove(); }, 200);
					}, 500);
				});
			var exit_path = path.exit().remove();

			// set the graph in motion
			force.start();
		};

		// insert new node at point
		this.addNode = function(node) {
			var node = {
				id: lastNodeId++,
				value: node,
				x: Math.random() * canvas.width / 2 + canvas.width / 4,
				y: Math.random() * canvas.height / 2 + canvas.height / 4
			};
			nodes.push(node);
		};

		this.removeNode = function(label) {
			var indexOf = indexOfNode(label);
			// Remove the node from the data list
			if (indexOf < 0) return;
			nodes.splice(indexOf, 1);
			// Remove the links adjacent to the node
			var toSplice = links.filter(function(l) {
				return (l.source.value.label === label || l.target.value.label === label);
			});
			toSplice.map(function(l) {
				links.splice(links.indexOf(l), 1);
			});
		};

		// this.highlightNode = function(label, color) {
		// 	var indexOf = indexOfNode(label);
		// 	if (indexOf < 0) return;
		// };

		// this.deselectNode = function(label, color) {
		// 	var indexOf = indexOfNode(label);
		// 	if (indexOf < 0) return;
		// };

		// this.accessNodeField = function(label, field) {
		// 	var indexOf = indexOfNode(label);
		// 	if (indexOf < 0) return;

		// };

		// this.highlightEdge = function(label) {
		// 	var indexOf = indexOfEdge(label);
		// 	if (indexOf < 0) return;
		// };

		// this.accessEdgeField = function(label, field) {
		// 	var indexOf = indexOfEdge(label);
		// 	if (indexOf < 0) return;
		// };

		this.getNode = function(label) {
			var indexOf = indexOfNode(label);
			if (indexOf < 0) return;
			nodes[indexOf].selected = true;
		};

		this.getNodes = function() {
			for (var i = 0; i < nodes.length; i++) {
				nodes[i].selected = true;
			}
		};

		this.getEdge = function(l1, l2) {
			for (var i = 0; i < links.length; i++) {
				if (links[i].source.value.label == l1 && links[i].target.value.label == l2) {
					links[i].selected = true;
					break;
				}
			}
		};

		this.setEdge = function(l1, l2, n3) {
			var indexOf = indexOfNode(l1);
			if (indexOf < 0) return;
			var n1 = nodes[indexOf]
			indexOf = indexOfNode(l2);
			if (indexOf < 0) return;
			var n2 = nodes[indexOf]
			// TODO: Check for duplicate edge?
			var link = {source: n1, target: n2, left: false, right: true, value: n3};
			links.push(link);
		};

		this.removeEdge = function(l1, l2) {
			for (var i = 0; i < links.length; i++) {
				if (links[i].source.value.label == l1 && links[i].target.value.label == l2) {
					links[i].removed = true;
					links[i].removed_index = i;
					break;
				}
			}
		};

		this.getNeighbors = function(node) {
			this.getNode(node.label);
			for (var i = 0; i < links.length; i++) {
				if (links[i].source.value.label == node.label) {
					links[i].selected = true;
					// links[i].target.highlight = true;
				}
			}
		};

		function indexOfNode(label) {
			var indexOf = -1;
			for (var i = 0; i < nodes.length; i++) {
				if (nodes[i].value.label == label) {
					indexOf = i;
				}
			}
			return indexOf;
		}

		function indexOfEdge(label) {
			var indexOf = -1;
			for (var i = 0; i < nodes.length; i++) {
				if (nodes[i].value.label == label) {
					indexOf = i;
				}
			}
			return indexOf;
		}
	};

	return Graph;
});