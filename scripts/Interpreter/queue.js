define(function() {
	var Queue = function(canvas) {
		// Data references
		var nodes = [];
		var links = [];

		// SVG groups
		var svg = canvas.svg[0];
		// var blocks = d3.select(svg).append("g").attr("class", ".cv-nodes").selectAll("rect");
		// var labels = d3.select(svg).append("g").attr("class", ".cv-labels").selectAll("text");
		// var text_1 = d3.select(svg).append("g").attr("class", ".cv-text").selectAll("text");
		// var text_2 = d3.select(svg).append("g").attr("class", ".cv-text").selectAll("text");
		// var bottom = d3.select(svg).append("rect").attr("class", "cv-queue-bottom");
		// var right = d3.select(svg).append("rect").attr("class", "cv-queue-right");
		// var left =d3.select(svg).append("rect").attr("class", "cv-queue-left");
		var selectionForUpdate;
		var enterSelection;
		var exitSelection;
		var isDequeueOp = false;
		var isPeekOp = false;

		this.enqueue = function(node) {
			var start = nodes.length - 1;
			nodes.push(node);
			var end = nodes.length - 1;
			links.push({source: start, target: end, left: false, right: true });
		};
		this.dequeue = function() {
			nodes.shift();
			links.shift();
			isDequeueOp = true;

		};
		this.peek = function() {
			isPeekOp = true;
		};
		var oldNodes; //reference to previously created nodes
		this.resize = function() {
			// create svg
			var svgWidth = canvas.width;
			var svgHeight = canvas.height;
			var defaultWidth = 20;
			var nodeWidth = defaultWidth;

			// Border
			var topBorderWidth = svgWidth/2, botBorderWidth = svgWidth/2;
			var topBorderHeight = 3, botBorderHeight = 3;
			var frontBorderWidth = 3;
			var frontBorderHeight = svgHeight;

			// Node constant
			var padding = 1;
			var nodeHeight = svgHeight - 2 * (padding + topBorderHeight);

			var nodeY = padding + topBorderHeight;
			var nodeX = 0;

			// Border
			var topBorder = d3.select(svg).append("rect").attr({
			  x: function() {
			    return svgWidth / 2;
			  },
			  y: function() {
			    return 0;
			  },
			  width: function() {
			    return topBorderWidth;
			  },
			  height: function() {
			    return topBorderHeight;
			  }
			}).style("fill", "red");
			var botBorder = d3.select(svg).append("rect").attr({
			  x: function() {
			    return svgWidth / 2;
			  },
			  y: function() {
			    return svgHeight - botBorderHeight;
			  },
			  width: function() {
			    return botBorderWidth;
			  },
			  height: function() {
			    return botBorderHeight;
			  }
			}).style("fill", "red");
			var frontBorder = d3.select(svg).append("rect").attr({
			  x: function() {
			    return svgWidth - frontBorderWidth;
			  },
			  y: function() {
			    return 0;
			  },
			  width: function() {
			    return frontBorderWidth;
			  },
			  height: function() {
			    return frontBorderHeight;
			  }
			}).style("fill", "red");

			var maxNumNodes = (svgWidth/2)  / nodeWidth;

			animate();
			if (isDequeueOp) {
				reposition();
				isDequeueOp = false;
			}
			if (isPeekOp) {
				peekAnimation();
				isPeekOp = false;
			}

			function peekAnimation() {
				if (nodes.length > 0) {
					selectionForUpdate.each(function(d, i) {
						if (i !== 0) {
							d3.select(this).transition().duration(1000).style("fill-opacity", "0");
						} else {
							d3.select(this).transition().duration(1000).style("stroke-width", "100");
							d3.select(this).transition().duration(1000).style("stroke", "rgb(0,0,0)");
						}
					}).transition().delay(500).each(function(d, i) {
						if (i !== 0) {
							d3.select(this).transition().delay(1000).duration(1000).style("fill-opacity", 1);
						} else {
							d3.select(this).transition().duration(1000).style("stroke-width", "0");
							d3.select(this).transition().duration(1000).style("stroke", "rgb(0,0,0)");
						}
					});
				}
			}
			function reposition() {
			  selectionForUpdate = d3.select(svg).selectAll(".node").transition().attr({
			    x: function() {
			      return parseInt(this.getAttribute("x")) + nodeWidth;
			    },
			    y: function() {
			      return nodeY;
			    },
			    width: function() {
			      return nodeWidth;
			    },
			    height: function() {
			      return nodeHeight;
			    }
			  });
			}

			function animate() {
			  if (nodes.length >= maxNumNodes) {
			    nodeWidth = (svgWidth / 2) / nodes.length;
			    selectionForUpdate.transition().duration(500).attr({
			      x: function (d, i) {
			        return svgWidth - frontBorderWidth - nodeWidth * (i+1);
			       },
			      y: function (d, i) {
			        return nodeY;
			       },
			      width: function (d, i) {
			         return nodeWidth;
			        },
			      height: function (d, i) {
			        return nodeHeight;
			       }
			    });
			  }

			  // selectionForUpdate = d3.select(svg).selectAll(".node").data(nodes, function(d) {return d.label;});
				selectionForUpdate = d3.select(svg).selectAll(".node").data(nodes, function(d) {
					return d.nodeId;
				});
			  enterSelection = selectionForUpdate.enter().append("rect");
			  enterSelection.classed({'node': true});
			  enterSelection.attr({
			    x: function() {
			      return nodeX;
			    },
			    y: function() {
			      return nodeY;
			    },
			    width: function() {
			      return nodeWidth;
			    },
			    height: function() {
			      return nodeHeight;
			    }
			  }).style("fill", "rgb(213, 216, 214)");
			  enterSelection.transition().ease("cubic-in").duration(100).attr({
			    x: function (d, i) {
			      return svgWidth - frontBorderWidth - nodeWidth * (i+1);
			     },
			    y: function (d, i) {
			      return nodeY;
			     },
			    width: function (d, i) {
			       return nodeWidth;
			      },
			    height: function (d, i) {
			      return nodeHeight;
			     }
			   }).each("end", function() {
			     var r = Math.random() * 255;
			     var g = Math.random() * 255;
			     var b = Math.random() * 255;
			     var color = "rgb(" + r + "," + g + "," + b + ")";
			    d3.select(this).transition().duration(1000).style("fill", color);
			  });
				exitSelection = selectionForUpdate.exit().remove();
			}
		};
	};

	return Queue;
});