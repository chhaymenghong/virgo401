define(function() {
	var Stack = function(canvas) {
		// Data references
		var nodes = [];
		var links = [];
		var isPeekOp = false;

		// SVG groups
		var svg = d3.select(canvas.svg[0]).attr("oncontextmenu", "return false;");
		var blocks = svg.append("g").attr("class", ".cv-nodes").selectAll("rect");
		var labels = svg.append("g").attr("class", ".cv-labels").selectAll("text");
		var text_1 = svg.append("g").attr("class", ".cv-text").selectAll("text");
		var text_2 = svg.append("g").attr("class", ".cv-text").selectAll("text");
		var bottom = svg.append("rect").attr("class", "cv-stack-bottom");
		var right = svg.append("rect").attr("class", "cv-stack-right");
		var left = svg.append("rect").attr("class", "cv-stack-left");

		// Some constants
		var bar_width = 5;
		var max_width = 300;
		var def_node_height = 25;

		this.push = function(node) {
			var start = nodes.length - 1;
			nodes.push(node);
			var end = nodes.length - 1;
			links.push({source: start, target: end, left: false, right: true });
		};

		this.pop = function(node) {
			nodes.pop();
			links.pop();
		};
		this.peek = function() {
			isPeekOp = true;
		}

		this.resize = function() {
			var width = canvas.width;
			var height = canvas.height;
			var padding_sides = Math.max(width * 0.2, (width - max_width) / 2);
			var padding_top = height * 0.3;
			var padding_bottom = height * 0.1;
			var stack_width =  width - padding_sides * 2;
			var stack_height = height - padding_top - padding_bottom;
			var node_width = stack_width - bar_width * 4;
			var node_height = Math.min(stack_height / nodes.length, def_node_height);

			function dispText() {
				return node_height < def_node_height * 0.7 ? 0 : 1;
			}
			function peekAnimation() {
				blocks.each(function(d, i) {
					if (i == nodes.length - 1) {
						d3.select(this).transition().duration(500).style("fill", "blue");
					}
				});
			}
			if (isPeekOp) {
				isPeekOp = false;
				peekAnimation();
			}


			// Draw the stack container
			bottom
				.attr("x", padding_sides)
				.attr("y", height - padding_bottom)
				.attr("width", stack_width)
				.attr("height", bar_width);
			left
				.attr("x", padding_sides)
				.attr("y", padding_top)
				.attr("width", bar_width)
				.attr("height", stack_height);
			right
				.attr("x", width - padding_sides - bar_width)
				.attr("y", padding_top)
				.attr("width", bar_width)
				.attr("height", stack_height);

			// Select the nodes
			blocks = blocks.data(nodes);
			labels = labels.data(nodes);
			text_1 = text_1.data(nodes);
			text_2 = text_2.data(nodes);

			// Update
			blocks
				.attr("x", padding_sides + bar_width * 2)
				.attr("y", function(d, i) {
					return height - padding_bottom - node_height * (i + 1);
				})
				.attr("width", node_width)
				.attr("height", node_height)
				.style("fill-opacity", 1);
			labels
				.attr("x", padding_sides + bar_width * 2 - 20)
				.attr("y", function(d, i) {
					return height - padding_bottom - node_height * i - node_height / 2 + bar_width / 2;
				})
				.style("fill-opacity", dispText);
			text_1
				.attr("x", function(d) {
					var keys = Object.keys(d.fields);
					if (keys.length > 1)
						return padding_sides + node_width / 3;
					else
						return padding_sides + node_width / 2;
				})
				.attr("y", function(d, i) {
					return height - padding_bottom - node_height * i - node_height / 2 + bar_width / 2;
				})
				.style("fill-opacity", dispText);
			text_2
				.attr("class", "cv-stack-text")
				.attr("x", function(d) {
					var keys = Object.keys(d.fields);
					if (keys.length > 1)
						return padding_sides + node_width / 3 * 2;
					else
						return 9000;
				})
				.attr("y", function(d, i) {
					return height - padding_bottom - node_height * i - node_height / 2 + bar_width / 2;
				})
				.style("fill-opacity", dispText);

			// Enter
			var nodesEnter = blocks.enter()
				.append("rect")
				.attr("class", "cv-node-rect")
				.attr("x", padding_sides + bar_width * 2)
				.attr("width", node_width)
				.attr("height", node_height)
				.attr("y", - node_height)
				.style("fill-opacity", 0)
				.transition()
				.ease("cubic-in")
				.duration(1000)
				.style("fill-opacity", 1)
				.attr("y", function(d, i) {
					return height - padding_bottom - node_height * (i + 1);
				});

			var labelsEnter = labels.enter()
				.append("text")
				.text(function(d) { return d.label; })
				.attr("class", "cv-stack-label")
				.attr("x", padding_sides + bar_width * 2 - 20)
				.attr("y", - node_height / 2 + bar_width / 2)
				.style("fill-opacity", 0)
				.transition()
				.ease("cubic-in")
				.duration(1000)
				.style("fill-opacity", dispText)
				.attr("y", function(d, i) {
					return height - padding_bottom - node_height * i - node_height / 2 + bar_width / 2;
				});

			var textEnter_1 = text_1.enter()
				.append("text")
				.text(function(d) {
					var keys = Object.keys(d.fields);
					keys.sort();
					return d.fields[keys[0]];
				})
				.attr("class", "cv-stack-text")
				.attr("x", function(d) {
					var keys = Object.keys(d.fields);
					if (keys.length > 1)
						return padding_sides + node_width / 3;
					else
						return padding_sides + node_width / 2;
				})
				.attr("y", - node_height / 2 + bar_width / 2)
				.call(enter);

			var textEnter_2 = text_2.enter()
				.append("text")
				.text(function(d) {
					var keys = Object.keys(d.fields);
					keys.sort();
					return d.fields[keys[1]];
				})
				.attr("class", "cv-stack-text")
				.attr("x", function(d) {
					var keys = Object.keys(d.fields);
					if (keys.length > 1)
						return padding_sides + node_width / 3 * 2;
					else
						return 9000;
				})
				.attr("y", - node_height / 2 + bar_width / 2)
				.call(enter);

			function enter() {
				this.style("fill-opacity", 0)
					.transition()
					.ease("cubic-in")
					.duration(1000)
					.style("fill-opacity", dispText)
					.attr("y", function(d, i) {
						return height - padding_bottom - node_height * i - node_height / 2 + bar_width / 2;
					});
			}

			// Exit
			exit(blocks.exit());
			exit(labels.exit());
			exit(text_1.exit());
			exit(text_2.exit());

			function exit(selection) {
				selection
					.transition()
					.ease("cubic-in")
					.duration(750)
					.attr("y", - node_height)
					.style("fill-opacity", 0)
					.remove();
			}
		};
	};

	return Stack;
});