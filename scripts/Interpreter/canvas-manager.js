define(['interact'], function(Interact) {
	var CanvasManager = function(container) {
		var LIST_SIZE = 150;
		var MAX_DISPLAY = 3;
		var num_displays = 0;
		var ORIENTATIONS = {
			LANDSCAPE: "landscape",
			PORTRAIT: "portrait"
		};
		var counter = 0;
		this.canvases = {};

		this.Resize = function() {

		};

		this.NewCanvas = function(id, type) {
			num_displays++;
			var new_canvas = new Canvas(id, null, container);
			new_canvas.type = type;
			this.canvases[new_canvas.id] = new_canvas;
			var list_item = $('<li class="cv-li"></li>');
			var wrapper = $('<div class="cv-li-inner">'+type+': '+new_canvas.id+'</div>');
			wrapper.attr("cv-num", new_canvas.id);
			list_item.append(wrapper);
			this.list.append(list_item);
			SetDraggable(wrapper[0], "parent");
			if (num_displays > MAX_DISPLAY) {
				new_canvas.Hide();
			} else {
				// Figure out the new dimensions
				if (num_displays == 2)
					new_canvas.SetPosition(250 * 2, 0);
				if (num_displays == 3)
					new_canvas.SetPosition(250 * 4, 0);
			}
		}

		function SpawnCanvas(id, x, y) {
			var canvas = this.canvases[id];
			canvas.Show();
			canvas.SetPosition(x, y);
		}

		function GenUID() {
			return counter++;
		}

		// Empty the container
		container.empty();
		this.list = $("#cv-standby")
		this.list.empty();
		SetDropZone(container[0], SpawnCanvas);
	}

	var Canvas = function(identifier, parent, container) {
		this.parent = parent;
		this.child = null;
		this.svg = $('<svg id="cv-'+identifier+'" class="cv-canvas draggable"></svg>');
		this.width = 0;
		this.height = 0;
		this.id = identifier;
		container.append(this.svg);
		this.position = this.svg.position();

		this.AddPartition = function(new_canvas) {

		};

		this.SetDimensions = function(width, height) {
			this.width = width;
			this.height = height;
			this.svg.width(width);
			this.svg.height(height);
		};

		this.SetPosition = function(left, top) {
			this.position = {left: left, top: top};
			this.svg.css('left', left);
			this.svg.css('top', top);
		};

		this.Hide = function() {
			this.svg.css('display', 'none');
		};

		this.Show = function() {
			this.svg.css('display', null);
		};

		SetDraggable(this.svg[0], "none");
	};

	// Drag and Drop functionality
	function SetDraggable(element, restrict) {
		Interact(element).draggable({
			// enable inertial throwing
			inertia: true,
			// keep the element within the area of it's parent
			restrict: {
				restriction: restrict,
				endOnly: true,
				elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
			},
			// enable autoScroll
			autoScroll: false,
			onstart: function(event) {
			},
			// call this function on every dragmove event
			onmove: dragMoveListener,
			// call this function on every dragend event
			onend: function(event) {

			}
		})
		// .resizable({
		// 	preserveAspectRatio: true,
		// 	edges: { left: true, right: true, bottom: true, top: true }
		// })
		// .on('resizemove', function (event) {
		// 	var target = event.target,
		// 	x = (parseFloat(target.getAttribute('data-x')) || 0),
		// 	y = (parseFloat(target.getAttribute('data-y')) || 0);

		// 	console.log(target);

		// 	// translate when resizing from top or left edges
		// 	x += event.deltaRect.left;
		// 	y += event.deltaRect.top;

		// 	target.style.webkitTransform = target.style.transform =
		// 	'translate(' + x + 'px,' + y + 'px)';

		// 	target.setAttribute('data-x', x);
		// 	target.setAttribute('data-y', y);
		// });

		function dragMoveListener (event) {
			if(event.interaction.downEvent.button != 0) return false;

			var target = event.target,
			// keep the dragged position in the data-x/data-y attributes
			x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
			y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

			// translate the element
			target.style.webkitTransform = target.style.transform =	'translate(' + x + 'px, ' + y + 'px)';

			// update the position attributes
			target.setAttribute('data-x', x);
			target.setAttribute('data-y', y);
		}
	}

	function SetDropZone(element, callback) {
		Interact(element).dropzone({
			accept: 'div.cv-li-inner',
			overlap: 0.25,
			ondropactivate: function(event) {
				// event.target.classList.add('drop-active');
			},
			ondragenter: function (event) {
				var draggableElement = event.relatedTarget,
				dropzoneElement = event.target;

				// feedback the possibility of a drop
				dropzoneElement.classList.add('drop-target');
				draggableElement.classList.add('can-drop');
			},
			ondragleave: function (event) {
				// remove the drop feedback style
				event.target.classList.remove('drop-target');
				event.relatedTarget.classList.remove('can-drop');
			},
			ondrop: function (event) {
				console.log(event);
				console.log(event.pageX, event.pageY);
				if(callback) callback(event.pageX, event.pageY);
			},
			ondropactivate: function (event) {
				// remove active dropzone feedback
				event.target.classList.remove('drop-active');
				event.target.classList.remove('drop-target');
			}
		});
	}

	return CanvasManager;
});