// Ford Fulkerson Algorithm

// Intitialize flow network
var graph = new _v.Graph();
var r_graph = new _v.Graph();
var s = AddVertex("s");
var o = AddVertex("o");
var p = AddVertex("p");
var q = AddVertex("q");
var r = AddVertex("r");
var t = AddVertex("t");
AddEdge(s, o, 3);
AddEdge(s, p, 3);
AddEdge(o, p, 2);
AddEdge(o, q, 3);
AddEdge(p, r, 2);
AddEdge(r, t, 3);
AddEdge(q, r, 4);
AddEdge(q, t, 2);
// Find the max flow
console.log(MaxFlow(s, t));

// Helper function creates an N401 node and inserts it into graph
function AddVertex(v) {
    r_graph.addNode(new _v.Node(v));
    return graph.addNode(new _v.Node(v));
}

// Helper function takes two N401 nodes and creates a flow from n1 to n2 with capacity
function AddEdge(n1, n2, capacity) {
    // Create a flow from n1 to n2
    var edge = new _v.Node();
    graph.setEdge(n1, n2, edge);
    edge.set("f1", "C: " + capacity);
    edge.set("f2", "F: 0");
    edge.capacity = capacity;
    edge.flow = 0;
    edge.n1 = n1;
    edge.n2 = n2;

    // Create a residual edge from n2 to n1
    var r_edge = new _v.Node();
    r_graph.setEdge(n2, n1, r_edge);
    r_edge.set("f1", "R: " + capacity);
    r_edge.residual = capacity;
    r_edge.n1 = n2;
    r_edge.n2 = n1;
}


// Find an augmenting path to the sink
function FindPath(source, sink) {
    var s = r_graph.getNode(source.label);
    var t = r_graph.getNode(sink.label);
    // Initialize all the nodes
    var nodes = r_graph.getNodes();
    for (var i in nodes) {
        nodes[i].set("visited", "false");
        nodes[i].set("parent", "null");
        nodes[i].parent = null;
        nodes[i].visited = false;
        nodes[i].deselect();
    }
    var q = new _v.Queue();
    q.enqueue(t);

    while (!q.isEmpty()) {
        var curr = q.dequeue();
        if (curr.visited) continue;
        curr.select("orange");
        curr.set("visited", "true");
        curr.visited = true;

        var neighbors = r_graph.neighbors(curr);
        for (var i in neighbors) {
            var n = neighbors[i];
            var r_edge = r_graph.getEdge(curr, n);
            n.select("yellow");

            if (!n.visited && r_edge.residual > 0) {
                n.set("parent", curr.label);
                n.parent = curr;
                q.enqueue(n);
                
                // If curr.label is the sink, we're done
                if (n.label == s.label)
                    return Trace(t, s);
            }

            // When I'm done with n, I don't want it to be highlighted anymore
            n.deselect();
        }

        //  I'm now done with the curr node, so I want to color it red
        curr.select("red");
    }

    return [];
}

function Trace(start, end) {
    var path = [];
    var curr = end;
    while(curr != start) {
        console.log(curr);
        var r_edge = r_graph.getEdge(curr.parent, curr);
        r_edge.select("red");
        path.push(r_edge);
        curr = curr.parent;
    }
    return path;
}

function MaxFlow(source, sink) {
    // Continue the algorithm until no more augmenting paths can be found
    var path = FindPath(source, sink);
    while(path.length > 0) {
        // Calculate the bottleneck residual on the path
        // Use a Array401 object to see the animation of this happening
        var max_residual = Infinity;
        var max_edge = null;
        for (var i in path) {
            var r_edge = path[i];
            var residual = r_edge.residual;
            r_edge.get("f1");
            if (residual < max_residual) {
                max_edge = r_edge;
                max_residual = residual;
            }
        }
        var flow = max_residual;
        max_edge.select("green");

        // TODO:
        // Push the flow to each edge in the path,
        // subtract it from each residual edge
        var edges = [];
        for (var i in path) {
            var r_edge = path[i];
            var edge = graph.getEdge(r_edge.n2, r_edge.n1);
            edges.push(edge);

            var curr_flow = edge.flow;
            edge.flow = curr_flow + flow;
            edge.select("blue");
            edge.set("f2", "F: " + edge.flow);

            r_edge.residual = r_edge.residual - flow;
            r_edge.set("f1", "R: " + r_edge.residual);
            r_edge.deselect();
        }

        for (var i in edges) edges[i].deselect();
        // Find more augmenting paths
        path = FindPath(source, sink);
    }

    var sum = 0;
    var src_nodes = graph.neighbors(source);
    source.select("green");
    for (var i = 0; i < src_nodes.length; i++) {
        var edge = graph.getEdge(source, src_nodes[i]);
        edge.select("green");
        sum += edge.flow;
        edge.get('f2');
    }
    return sum;
}

// Returns the minimum node in an Array401 object sorted by node.label
function FindMin(array401) {
    var min = Infinity;
    var min_node = null;
    for (var i = 0; i < array401.length; i++) {
        var node = array401.get(i);
        var val = node.label;
        if (val < min) {
            min = val;
            min_node = node;
        }
    }
    return min_node;
}