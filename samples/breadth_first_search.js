// Construct graph
(function(){
  var graph = new _v.Graph();
  var rootNode = new _v.Node("root");
  graph.addNode(rootNode);
  var leftC1 = new _v.Node("hong");
  graph.addNode(leftC1);
  var rightC1 = new _v.Node("francis");
  graph.addNode(rightC1);

    // set edge
  var edgeRootToLeftC1 = new _v.Node("awesome");
  graph.setEdgeD(rootNode, leftC1, edgeRootToLeftC1);

  // var edges = graph.getEdge(rootNode, leftC1);

  var edgeRootToLeftC2 = new _v.Node("awesome");
  graph.setEdgeD(rootNode, rightC1, edgeRootToLeftC2);

  var allNodes = graph.getNodes();
  console.log(allNodes);
  var len = allNodes.length;
  // Initialize fields
  for (var i = 0; i < len; i++) {
    var node = allNodes[i];
    console.log(node);
    node.parent = null;
    node.set("distance", Infinity);
    node.set("parent", "");
  }
  // create empty queue
  var queue = new _v.Queue();
  console.log(queue);
  rootNode.set("distance", 0);
  queue.enqueue(rootNode);

  while (!queue.isEmpty()) {
    var currentNode = queue.dequeue();
    currentNode.select();
    var neighbors = graph.neighbors(currentNode);
    for (var j in neighbors) {
      var n = neighbors[j];
      n.select("grey");

      // Animations will play out on the fields being manipulated
      if (n.get("distance") == Infinity) {
        n.set("distance", currentNode.get("distance") + 1);
        n.set("parent", currentNode.label);
        n.parent = currentNode;
        queue.enqueue(n);
      }
      n.deselect();
    }
    currentNode.select("#000");
  }
})();
