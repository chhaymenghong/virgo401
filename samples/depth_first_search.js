// Create the Graph and Nodes
// Note: no visualization is played for these on new,
// only when they are added to the graph.
var graph = new _v.Graph();
var rootNode = new _v.Node("root");
graph.addNode(rootNode);
var leftC1 = new _v.Node("hong");
graph.addNode(leftC1);
var rightC1 = new _v.Node("francis");
graph.addNode(rightC1);
var c1LeftChild = new _v.Node("h-kid1");
var c1RightChild = new _v.Node("h-kid2");
graph.addNode(c1LeftChild);
graph.addNode(c1RightChild);
var c2LeftChild = new _v.Node("f-kid1");
var c2RightChild = new _v.Node("f-kid2");
graph.addNode(c2LeftChild);
graph.addNode(c2RightChild);

// Set the edges
// Note: we could optionally pass in an edge Node,
// which can display up to 2 fields (not the label).
graph.setEdge(rootNode, leftC1);
graph.setEdge(rootNode, rightC1);
graph.setEdge(leftC1, c1LeftChild);
graph.setEdge(leftC1, c1RightChild);
graph.setEdge(rightC1, c2LeftChild);
graph.setEdge(rightC1, c2RightChild)

// Initialize all the nodes
var nodes = graph.getNodes();
for (var i in nodes) {
    // Note: by using the set/get of the node,
    // we can display up to 2 fields + the label.
    nodes[i].set("distance", "Infinity");
    nodes[i].set("parent", "null");
    // Remember that we are creating a visualization,
    // so any fields/labels should be optimal for displaying.
    // Anything that is regular JS, including hiding fields in this node,
    // will not be visualized. 
    nodes[i].parent = null;
}

// Begin at the root node
rootNode.set("distance", 0);

// Create a new stack data structure
var s = new _v.Stack();
s.push(rootNode);

// For DFS, we use a stack to traverse.
while (!s.isEmpty()) {
    // I want to indicate that I am examining curr, so I highlight it orange.
    var curr = s.pop();
    curr.select("orange");

    // Loop through all the nodes adjacent to curr
    var neighbors = graph.neighbors(curr);
    for (var i in neighbors) {
        var n = neighbors[i];
        // Access the edge to trigger a visual
        graph.getEdge(curr, n);
        // Let's indicate that we're examining the neighbor
        n.select("yellow");

        // Animations will play out on the fields each time they are
        // accessed by get, or modified with set.
        if (n.get("distance") == "Infinity") {
            n.set("distance", curr.get("distance") + 1);
            n.set("parent", curr.label);
        }

        // When I'm done with n, I don't want it to be highlighted anymore
        n.deselect();
        s.push(n);
    }

    //  I'm now done with the curr node, so I want to color it red
    curr.select("red");
}