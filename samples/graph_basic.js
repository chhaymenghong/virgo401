// Welcome! Please read the language reference on the right for assistance.
// Graph Demo:
var g = new _v.Graph();
var a = new _v.Node("A");
var b = new _v.Node("B");
var c = new _v.Node("C");
var e = new _v.Node();
g.addNode(a);
a.set("f1", "ni hao");
a.set("f2", "shijie");
a.set("f3", "dont show");
g.addNode(b);
g.addNode(c);
a.select("pink");
g.setEdge(a, b);
g.setEdge(a, c, e);
var x = g.getEdge(a, c);
x.set("field1", "Hello");
x.set("field2", "World");
x.get("field1");
g.neighbors(a);
g.getNode("A").deselect();
g.getNodes();
g.removeEdge(a, c);
g.removeNode(a);
g.setEdge(b, c);
g.setEdge(c, b);
g.removeNode(b);
g.removeNode(c);